<?php

namespace App\Http\Resources\Admin\Marque\Collection;

use Illuminate\Http\Resources\Json\JsonResource;

class MarqueCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'=> (string) $this->name,
            'slug'=> (string) $this->slug,
            'link'=>[
                'self'=>Route('admin.marques.show',$this->id),
            ],
        ];
    }
}
