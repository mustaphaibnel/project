<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Reservation::class, function (Faker $faker) {
    return [
    'gps'=>$faker->boolean(50),
    'conducteur'=>$faker->boolean(50),
    'siege_bebe'=>$faker->boolean(50),
    'date_debut'=>'2018-05-22 01:54:23',
    'date_fin'=>'2018-05-26 01:54:23',
    'validate_at'=>'2018-05-29 01:54:23',
    'user_id'=>\App\User::all()->random()->id,
    'vehicule_id'=> App\Models\Vehicule::all()->random()->id,
    'client_id'=>App\Models\Client::all()->random()->id,
    ];
});
