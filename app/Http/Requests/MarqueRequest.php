<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MarqueRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //dd($this->request->Attributes->name);


        //$this->request->replace(['name'=>'some text','slug'=>'some slug']);
        //($this->request);
        return [
            'some_name' => 'required',
            'some_slug' => 'required',
        ];
    }


    public function attributes()
    {
        return [
            'some_name' => 'le nom',
            'some_slug' => 'the slug',
        ];
    }
    protected function validationData()
    {
        return array_merge($this->request->all(), [
            'some_name' => $this->request->get('name'),
            'some_slug' => $this->request->get('slug'),
        ]);
    }
}
