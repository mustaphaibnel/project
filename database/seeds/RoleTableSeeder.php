<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(
            [
                [
                    'name'=>'admin',
                    'slug'=>'admin',
                    'description'=>'Super Admin',
                ],
                [
                    'name'=>'chef',
                    'slug'=>'chef',
                    'description'=>'chef d agence ',
                ],
                [
                    'name'=>'employee',
                    'slug'=>'employee',
                    'description'=>' employee ',
                ],
                [
                    'name'=>'responsable',
                    'slug'=>'responsable',
                    'description'=>' responsable ',
                ],
                [
                    'name'=>'consultant',
                    'slug'=>'consultant',
                    'description'=>' client  ',
                ],
            ]
        );
    }
}
