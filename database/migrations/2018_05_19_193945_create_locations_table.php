<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('locations', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('gps');
            $table->boolean('conducteur');
            $table->boolean('siege_bebe');
            $table->dateTime('date_debut');
            $table->dateTime('date_fin');
            $table->dateTime('prolongation')->nullable();
            $table->dateTime('validate_at')->nullable();
            $table->integer('validate_by')->nullable();
            $table->unsignedInteger('vehicule_id');
            $table->unsignedInteger('client_id');
            $table->unsignedInteger('user_id');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('locations');
    }
}
