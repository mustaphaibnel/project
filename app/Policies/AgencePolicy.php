<?php

namespace App\Policies;

use App\User;
use App\Models\Agence;
use Illuminate\Auth\Access\HandlesAuthorization;

class AgencePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the agence.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Agence  $agence
     * @return mixed
     */
    public function view(User $user, Agence $agence)
    {
        //
    }


    /**
     * Determine whether the user can create agences.
     *
     * @param  \App\User  $user
     * @return mixed
     */
    public function create(User $user)
    {
        //
    }
    public function show(User $user, Agence $agence)
    {
        return $user->ownedAgence($agence->id);
    }
    public function users(User $user, Agence $agence)
    {
        return $user->ownedAgence($agence->id);
    }
    public function vehicules(User $user, Agence $agence)
    {
        return $user->ownedAgence($agence->id);
    }
    public function clients(User $user, Agence $agence)
    {
        return $user->ownedAgence($agence->id);
    }
    public function reservations(User $user, Agence $agence)
    {
        return $user->ownedAgence($agence->id);
    }
    public function locations(User $user, Agence $agence)
    {
        return $user->ownedAgence($agence->id);
    }
    public function garages(User $user, Agence $agence)
    {
        return $user->ownedAgence($agence->id);
    }
    public function modeles(User $user, Agence $agence)
    {
        return $user->ownedAgence($agence->id);
    }
    public function marques(User $user, Agence $agence)
    {
        return $user->ownedAgence($agence->id);
    }
    /**
     * Determine whether the user can update the agence.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Agence  $agence
     * @return mixed
     */
    public function update(User $user, Agence $agence)
    {
        //
    }

    /**
     * Determine whether the user can delete the agence.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Agence  $agence
     * @return mixed
     */
    public function delete(User $user, Agence $agence)
    {
        //
    }
}
