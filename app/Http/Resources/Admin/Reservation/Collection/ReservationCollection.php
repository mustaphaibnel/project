<?php

namespace App\Http\Resources\Admin\Reservation\Collection;

use Illuminate\Http\Resources\Json\JsonResource;

class ReservationCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'client'=>$this->client->name,
            'vehicule'=>$this->vehicule->modele->name,
            'status'=>$this->validate_at?'valider le '.$this->validate_at:'non valider',
            'links'=>[
                'self'=>Route('admin.reservations.show',$this->id),
            ],
        ];
    }
}
