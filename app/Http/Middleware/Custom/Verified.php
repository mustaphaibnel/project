<?php

namespace App\Http\Middleware\Custom;


use Closure;

class Verified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->verified==Null){
            return response([['errors'=>"Unauthorized action.",'details'=>["type"=> 'unverified',"message"=> 'utilisateur non verifier']]], 403);
        }
        else{
            return $next($request);
        }
    }
}
