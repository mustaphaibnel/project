<?php

namespace App\Models;

use App\User;

class Client extends User
{
    public function reservations(){
        return $this->hasMany(Reservation::class);
    }

    public function locations(){
        return $this->hasMany(Location::class);
    }
    public function ownedReservation($id){

        return $this->reservations()->contains('id',$id);

    }
    public function ownedLocation($id){

        return $this->locations()->contains('id',$id);

    }
}
