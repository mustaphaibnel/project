<?php

namespace App\Http\Resources\Admin\Marque\Resource;

use Illuminate\Http\Resources\Json\JsonResource;

class MarqueResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
         return [
        'name'=> (string) $this->name,
        'slug'=> (string) $this->slug,
        'created_at'=>(string) $this->created_at->diffForHumans(),
        'updated_at'=>(string) $this->updated_at->diffForHumans(),
        'link'=>[
            'self'=>Route('admin.marques.show',$this->id),
            'modeles'=>Route('admin.marques.modeles',$this->id),
            'image'=>$this->image,
            'vehicules'=>Route('admin.marques.vehicules',$this->id),
            'agences'=>Route('admin.marques.agences',$this->id),
            'garages'=>Route('admin.marques.garages',$this->id),
        ],
    ];
    }
}
