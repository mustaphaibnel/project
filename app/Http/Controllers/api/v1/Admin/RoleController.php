<?php

namespace App\Http\Controllers\api\v1\Admin;


use App\Models\Role;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $roles=Role::all();
        $this->authorize('index', Role::class);
        return Response()->json($roles);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {

        $this->authorize('store', Role::class);
        return Response()->json('ok from store');
    }


    public function show(Request $request,Role $role)
    {
        $role=Role::find($role)->first();
        $this->authorize('show',$role);
        return Response()->json($role);


    }


    public function edit(Role $role)
    {
        //
    }

    public function update(Request $request, Role $role)
    {
        //$role=Role::find($role);
       // $this->authorize('update', $role);

        $role=Role::find($role)->first();
        $this->authorize('update',$role);

        return Response()->json('from update');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Role  $role
     * @return \Illuminate\Http\Response
     */
    public function destroy(Role $role)
    {
        //
    }
}
