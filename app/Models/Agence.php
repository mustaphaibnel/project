<?php

namespace App\Models;

use App\Scopes\AgenceScope;
use App\User;
use App\Models\Reservation;
use App\Models\Location;


class Agence extends AgenceGarage
{

    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new AgenceScope);
    }

    public function garages(){
        return $this->hasMany(Garage::class,'agence_id');
    }

    public function users(){
        $data =  $this->garages()->with('users')->get()->pluck('users')->collapse()->unique('id')->all();
        return collect($data);
    }
    public function Vehicules()
    {
        return $this->hasManyThrough(Vehicule::class, Garage::class);
    }

    public function modeles(){

        return $modeles =Agence::find($this->id)->vehicules()->get()->pluck('modele')->all();

    }


    public function getMarquesAttribute(){

        return $marques =Agence::find($this->id)->vehicules()->with('modele.marque')->get()->pluck('modele.marque')->all();

    }

    public function reservations()
    {
        return $this->hasManyThrough(Reservation::class, Vehicule::class,'garage_id');
    }
    public function locations()
    {
        return $this->hasManyThrough(Location::class, Vehicule::class,'garage_id');
    }
}
