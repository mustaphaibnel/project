<?php

namespace App\Providers;

use App\Models\Agence;
use App\Models\Client;
use App\Models\Garage;
use App\Models\Location;
use App\Models\Marque;
use App\Models\Modele;
use App\Models\Permission;
use App\Models\Reservation;
use App\Models\Role;
use App\Models\Tariff;
use App\Models\Vehicule;
use App\Policies\AgencePolicy;
use App\Policies\ClientPolicy;
use App\Policies\GaragePolicy;
use App\Policies\LocationPolicy;
use App\Policies\MarquePolicy;
use App\Policies\ModelePolicy;
use App\Policies\PermissionPolicy;
use App\Policies\ReservationPolicy;
use App\Policies\RolePolicy;
use App\Policies\TariffPolicy;
use App\Policies\UserPolicy;
use App\Policies\VehiculePolicy;
use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Role::class => RolePolicy::class,
        Permission::class => PermissionPolicy::class,
        User::class => UserPolicy::class,
        Client::class => ClientPolicy::class,
        Agence::class => AgencePolicy::class,
        Garage::class => GaragePolicy::class,
        Marque::class => MarquePolicy::class,
        Modele::class => ModelePolicy::class,
        Vehicule::class => VehiculePolicy::class,
        Tariff::class => TariffPolicy::class,
        Location::class => LocationPolicy::class,
        Reservation::class => ReservationPolicy::class,

    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();


    }
}
