<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Marque extends Model
{
    public function modeles(){
        return $this->hasMany(Modele::class);
    }

    public function Vehicules()
    {
        return $this->hasManyThrough(Vehicule::class, Modele::class);
    }
    public function agences()
    {
        return $this->vehicules()->get()->pluck('agence');
    }
    public function garages()
    {
        return $this->vehicules->pluck('garage');
    }
    public function reservations()
    {
        return $reservations =Marque::find($this->id)->vehicules()->with('reservations')->get()->pluck('reservations')->unique('id')->all();

    }
    public function locations()
    {
        return $locations =Marque::find($this->id)->vehicules()->with('locations')->get()->pluck('locations')->collapse()->unique('id')->all();
    }
}
