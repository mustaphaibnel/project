<?php

namespace App\Http\Controllers\api\v1\Admin;


use App\Models\UserActivation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserActivationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\UserActivation  $userActivation
     * @return \Illuminate\Http\Response
     */
    public function show(UserActivation $userActivation)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\UserActivation  $userActivation
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, UserActivation $userActivation)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\UserActivation  $userActivation
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserActivation $userActivation)
    {
        //
    }
}
