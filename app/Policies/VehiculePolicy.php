<?php

namespace App\Policies;

use App\Models\Vehicule;
use App\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class VehiculePolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function show(User $user, Vehicule $vehicule)
    {
        return $user->ownedVehicule($vehicule->id);
    }
    public function reservations(User $user, Vehicule $vehicule)
    {
        return $user->ownedVehicule($vehicule->id);
    }
    public function locations(User $user, Vehicule $vehicule)
    {
        return $user->ownedVehicule($vehicule->id);
    }
}
