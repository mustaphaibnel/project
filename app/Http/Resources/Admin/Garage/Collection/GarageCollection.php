<?php

namespace App\Http\Resources\Admin\Garage\Collection;

use Illuminate\Http\Resources\Json\JsonResource;

class GarageCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'slug'=>$this->slug,
            'name'=>$this->name,
            'ville'=>$this->ville,
            'links'=>[
                'self'=>Route('admin.garages.show',$this->id),
            ],
        ];
    }
}
