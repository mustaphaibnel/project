<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVehiculesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('vehicules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('matricule');
            $table->string('couleur');
            $table->string('carburant');
            $table->integer('ports');
            $table->integer('places');
            $table->integer('kilometrage');
            $table->integer('min_jour');
            $table->string('niveaucarburant');
            $table->boolean('climatisation');
            $table->boolean('vitesse_automatique');
            $table->boolean('promotion');
            $table->timestamp('published_at')->nullable();
            $table->float('prix_siege_bebe');
            $table->float('prix_gps');
            $table->float('prix_conducteur');
            $table->unsignedInteger('garage_id');
            $table->unsignedInteger('modele_id');
            $table->string('image');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('vehicules');
    }
}
