<?php

namespace App\Http\Resources\Admin\Agence\Resource;

use Illuminate\Http\Resources\Json\JsonResource;

class AgenceResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'slug'=>$this->slug,
            'name'=>$this->name,
            'addresse'=>$this->addresse,
            'code_postale'=>$this->code_postale,
            'ville'=>$this->ville,
            'site_web'=>$this->site_web,
            'facebook_page'=>$this->facebook_page,
            'geolocalisation'=>$this->geolocalisation,
            'links'=>[
                'self'=>Route('admin.agences.show',$this->id),
                'image'=>$this->image,
                'garages'=>Route('admin.agences.garages',$this->id),
                'vehicules'=>Route('admin.agences.vehicules',$this->id),
                'modeles'=>Route('admin.agences.modeles',$this->id),
                'marques'=>Route('admin.agences.marques',$this->id),
                'users'=>Route('admin.agences.users',$this->id),
                'reservations'=>Route('admin.agences.reservations',$this->id),
                'locations'=>Route('admin.agences.locations',$this->id),
            ],
        ];
    }
}
