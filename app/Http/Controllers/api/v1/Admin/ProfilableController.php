<?php

namespace App\Http\Controllers\api\v1\Admin;

use App\Models\Profilable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfilableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Profilable  $profilable
     * @return \Illuminate\Http\Response
     */
    public function show(Profilable $profilable)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Profilable  $profilable
     * @return \Illuminate\Http\Response
     */
    public function edit(Profilable $profilable)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Profilable  $profilable
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profilable $profilable)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Profilable  $profilable
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profilable $profilable)
    {
        //
    }
}
