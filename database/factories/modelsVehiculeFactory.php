<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Vehicule::class, function (Faker $faker) {
    return [
    'matricule'=>$faker->bankAccountNumber,
    'couleur'=>$faker->colorName,
    'carburant'=>$faker->randomElement(array ('desiel','essence')),
    'ports'=>$faker->randomNumber(),
    'places'=>$faker->randomNumber(),
    'kilometrage'=>$faker->randomNumber(),
    'min_jour'=>$faker->randomNumber(),
    'niveaucarburant'=>$faker->randomElement(array('1/2','1/3','1/4','full')),
    'climatisation'=>$faker->boolean,
    'vitesse_automatique'=>$faker->boolean,
    'promotion'=>$faker->boolean,
    'published_at'=>$faker->dateTime,
    'prix_siege_bebe'=>0,
    'prix_gps'=>15,
    'prix_conducteur'=>200,
    'image'=>$faker->url,
    'garage_id'=>App\Models\Garage::all()->random()->id,
    'modele_id'=>App\Models\Modele::all()->random()->id,
    ];
});
