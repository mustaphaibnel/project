<?php

namespace App\Http\Controllers\api\v1\Admin;

use App\Http\Resources\Admin\Garage\Collection\GarageCollection;
use App\Http\Resources\Admin\Garage\Resource\GarageResource;
use App\Http\Resources\Admin\Location\Collection\LocationCollection;
use App\Http\Resources\Admin\Marque\Collection\MarqueCollection;
use App\Http\Resources\Admin\Modele\Collection\ModeleCollection;
use App\Http\Resources\Admin\Reservation\Collection\ReservationCollection;
use App\Http\Resources\Admin\Vehicule\Collection\VehiculeCollection;
use App\Models\Garage;
use App\Policies\GaragePolicy;
use App\User;
use http\Env\Response;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GarageController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $user=null;

    public function __construct()
    {
        $this->user=auth('api')->user();
    }

    public function index()
    {

        $garages= $this->user->garages()->paginate();

        return  GarageCollection::collection($garages);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Garage  $garage
     * @return \Illuminate\Http\Response
     */
    public function show($garageId)
    {
        $garage=Garage::find($garageId);
        if ($this->user->can('show', $garage)) {

            return new GarageResource($garage);
        }
        else{
            return response(["message"=> "Unauthorized action."], 403);
        }
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Garage  $garage
     * @return \Illuminate\Http\Response
     */
    public function edit(Garage $garage)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Garage  $garage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Garage $garage)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Garage  $garage
     * @return \Illuminate\Http\Response
     */
    public function destroy(Garage $garage)
    {
        //
    }
    public function users($garageId)
    {
        $garage=Garage::find($garageId);
        if ($this->user->can('users', $garage)) {

            return $garage->users;
        }
        else{
            return response('Unauthorized action.', 403);
        }

    }
    public function clients($garageId)
    {
        $garage=Garage::find($garageId);
        if ($this->user->can('clients', $garage)) {

            return $garage->clients;
        }
        else{
            return response('Unauthorized action.', 403);
        }

    }
    public function locations($garageId)
    {
        $garage=Garage::find($garageId);
        if ($this->user->can('locations', $garage)) {

            $locations= $garage->locations()->paginate();
            return  LocationCollection::collection($locations);
        }
        else{
            return response('Unauthorized action.', 403);
        }

    }
    public function reservations($garageId)
    {
        $garage=Garage::find($garageId);
        if ($this->user->can('reservations', $garage)) {

            $reservations= $garage->reservations()->paginate();
            return  ReservationCollection::collection($reservations);
        }
        else{
            return response('Unauthorized action.', 403);
        }

    }
    public function vehicules($garageId)
    {
        $garage=Garage::find($garageId);
        if ($this->user->can('vehicules', $garage)) {

            $vehicules= $garage->vehicules()->paginate();
            return  VehiculeCollection::collection($vehicules);
        }
        else{
            return response('Unauthorized action.', 403);
        }

    }
    public function modeles($garageId)
    {
        $garage=Garage::find($garageId);
        if ($this->user->can('modeles', $garage)) {

            $modeles= $garage->modeles();
            return  ModeleCollection::collection($modeles);
        }
        else{
            return response('Unauthorized action.', 403);
        }

    }
    public function marques($garageId)
    {
        $garage=Garage::find($garageId);
        if ($this->user->can('marques', $garage)) {

            $marques= $garage->marques();
            return MarqueCollection::collection($marques);
        }
        else{
            return response('Unauthorized action.', 403);
        }

    }
}
