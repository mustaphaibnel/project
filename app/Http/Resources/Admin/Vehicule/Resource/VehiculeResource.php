<?php

namespace App\Http\Resources\Admin\Vehicule\Resource;

use Illuminate\Http\Resources\Json\JsonResource;

class VehiculeResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'marque'=>$this->marque()->name,
            'modele'=>$this->modele->name,
            'garage'=>$this->garage->name,
            'agence'=>$this->garage->agence->name,
            'vitesse_automatique'=>$this->vitesse_automatique,
            'niveaucarburant'=>$this->niveaucarburant,
            'climatisation'=>$this->climatisation,
            'ports'=>$this->ports,
            'couleur'=>$this->couleur,
            'places'=>$this->places,
            'promotion'=>$this->promotion,
            'links'=>[
                'self'=>Route('admin.vehicules.show',$this->id),
                'image'=>$this->image,
                'reservations'=>Route('admin.vehicules.reservations',$this->id),
                'locations'=>Route('admin.vehicules.locations',$this->id),
                'agence'=>Route('admin.agences.show',$this->garage->agence->id),
                'garage'=>Route('admin.garages.show',$this->garage->id),
                'marque'=>Route('admin.marques.show',$this->marque()->id),
                'modele'=>Route('admin.modeles.show',$this->modele->id),
            ],
        ];
    }
}
