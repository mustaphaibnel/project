<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Marque::class, function (Faker $faker) {
    return [
        'name'=>$faker->company,
        'slug'=>'name',
        'image'=>$faker->url,
    ];
});
