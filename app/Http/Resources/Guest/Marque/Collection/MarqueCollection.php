<?php

namespace App\Http\Resources\Guest\Marque\Collection;

use Illuminate\Http\Resources\Json\JsonResource;

class MarqueCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'=> (string) $this->name,
            'slug'=> (string) $this->slug,
            'created_at'=>(string) $this->created_at->diffForHumans().'test',
            'updated_at'=>(string) $this->updated_at->diffForHumans(),
            'link'=>[
                'self'=>Route('admin.marques.show',$this->id),
            ],
        ];
    }
}
