<?php

namespace App\Http\Controllers\api\v1\Admin;

use App\Http\Resources\Admin\Agence\Collection\AgenceCollection;
use App\Http\Resources\Admin\Agence\Resource\AgenceResource;
use App\Http\Resources\Admin\Client\Collection\ClientCollection;
use App\Http\Resources\Admin\Garage\Collection\GarageCollection;
use App\Http\Resources\Admin\Location\Collection\LocationCollection;
use App\Http\Resources\Admin\Marque\Collection\MarqueCollection;
use App\Http\Resources\Admin\Modele\Collection\ModeleCollection;
use App\Http\Resources\Admin\Reservation\Collection\ReservationCollection;
use App\Http\Resources\Admin\User\Collection\UserCollection;
use App\Http\Resources\Admin\Vehicule\Collection\VehiculeCollection;
use App\Models\Agence;
use App\Models\Marque;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AgenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    private $user=null;

    public function __construct()
    {
        $this->user=auth('api')->user();
    }
    public function index()
    {
         $agences=$this->user->agences();


        return  AgenceCollection::collection($agences);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Agence  $agence
     * @return \Illuminate\Http\Response
     */
    public function show($agenceId)
    {
        $agence=Agence::find($agenceId);
        if ($this->user->can('show', $agence)) {

            return new AgenceResource($agence);
        }
        else{
            return response(["message"=> "Unauthorized action."], 403);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Agence  $agence
     * @return \Illuminate\Http\Response
     */
    public function edit(Agence $agence)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Agence  $agence
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Agence $agenceId)
    {

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Agence  $agence
     * @return \Illuminate\Http\Response
     */
    public function destroy(Agence $agence)
    {
        //
    }
    public function users($agenceId)
    {
        $agence=Agence::find($agenceId);
        if ($this->user->can('users', $agence)) {

            return $users= $agence->users();
        }
        else{
            return response('Unauthorized action.', 403);
        }

    }
    public function garages($agenceId)
    {
        $agence=Agence::find($agenceId);
        if ($this->user->can('garages', $agence)) {

             $agences=$agence->garages()->paginate();
            return  GarageCollection::collection($agences);
        }
        else{
            return response('Unauthorized action.', 403);
        }

    }
    public function locations($agenceId)
    {
        $agence=Agence::find($agenceId);
        if ($this->user->can('locations', $agence)) {

            $locations= $agence->locations()->paginate();
            return  LocationCollection::collection($locations);
        }
        else{
            return response('Unauthorized action.', 403);
        }

    }
    public function reservations($agenceId)
    {
        $agence=Agence::find($agenceId);
        if ($this->user->can('reservations', $agence)) {

            $reservations= $agence->reservations()->paginate();
            return  ReservationCollection::collection($reservations);
        }
        else{
            return response('Unauthorized action.', 403);
        }

    }
    public function vehicules($agenceId)
    {
        $agence=Agence::find($agenceId);
        if ($this->user->can('vehicules', $agence)) {

            $vehicules= $agence->vehicules()->paginate();
            return  VehiculeCollection::collection($vehicules);
        }
        else{
            return response('Unauthorized action.', 403);
        }

    }
    public function modeles($agenceId)
    {
        $agence=Agence::find($agenceId);
        if ($this->user->can('modeles', $agence)) {

            $modeles= $agence->modeles();
            return  ModeleCollection::collection($modeles);
        }
        else{
            return response('Unauthorized action.', 403);
        }

    }
    public function marques($agenceId)
    {
        $agence=Agence::find($agenceId);
        if ($this->user->can('marques', $agence)) {

            $marques= $agence->marques;
            $marques=Marque::hydrate($marques);
            dd($marques);
            return MarqueCollection::collection($marques);
        }
        else{
            return response('Unauthorized action.', 403);
        }

    }
    public function clients($agenceId)
    {
        $agence=Agence::find($agenceId);
        if ($this->user->can('clients', $agence)) {

            $clients= $agence->clients();
            return ClientCollection::collection($clients);
        }
        else{
            return response('Unauthorized action.', 403);
        }

    }
}
