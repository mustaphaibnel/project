<?php

namespace App\Http\Middleware\Custom;

use Closure;

class PermissionCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */


    public function handle($request, Closure $next)
    {
       $permissioncheck=$request->user()->hasPermission($request->route()->getName());

        if ($permissioncheck){
            return $next($request);
        }
        else {
            return response([['errors'=>"Unauthorized action.",'details'=>["type"=> 'permission',"message"=> 'utilisateur n a pas cette permission']]], 403);

        }
    }
}
