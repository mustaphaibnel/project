<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tariff extends Model
{
    public function vehicule(){
        return $this->belongsTo('App\Models\Vehicule');
    }
}
