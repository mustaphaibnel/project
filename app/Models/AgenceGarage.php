<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AgenceGarage extends Model
{
    protected $table='agences_garages';
}
