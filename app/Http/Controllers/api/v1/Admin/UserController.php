<?php

namespace App\Http\Controllers\api\v1\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */



    public function index()
    {
        $user=auth('api')->user();
        $users =$user->garages()->with('users')->get()->pluck('users')->collapse()->all();
        return $users;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
    public function agences($user)
    {
        $user=User::find($user)->first();
        return $user->garages()->with('agence')->get()->pluck('agence')->collapse()->all();
    }
    public function garages($user)
    {
        $user=User::find($user)->first();
        return $user->garages;
    }
    public function vehicules( $user)
    {
        $user=User::find($user)->first();
        return $user->vehicules();
    }
    public function locations($user)
    {
        $user=User::find($user)->first();
        return $user->locations();
    }
    public function reservations($user)
    {
        $user=User::find($user)->first();
        return $user->reservations();
    }
}
