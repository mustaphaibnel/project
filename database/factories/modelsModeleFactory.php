<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Modele::class, function (Faker $faker) {
    return [
        'name'=>$faker->company,
        'slug'=>'app',
        'marque_id'=>\App\Models\Marque::all()->random()->id,
    ];
});
