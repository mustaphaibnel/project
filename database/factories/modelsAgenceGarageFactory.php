<?php

use Faker\Generator as Faker;

$factory->define(App\Models\AgenceGarage::class, function (Faker $faker) {
    return [
        'name'=>$faker->company,
        'slug'=>$faker->slug,
        'addresse'=>$faker->address,
        'code_postale'=>$faker->postcode,
        'telephone_mobile'=>$faker->phoneNumber,
        'telephone_fix'=>$faker->phoneNumber,
        'ville'=>$faker->city,
        'site_web'=>'www'.$faker->name.'com',
        'facebook_page'=>'www.facebook.com/'.$faker->slug,
        'image'=>$faker->url,
        'geolocalisation'=>$faker->address,
        'activated'=>$faker->dateTime,
        'verified'=>$faker->dateTime,
        'agence_id'=>1,

    ];
});
