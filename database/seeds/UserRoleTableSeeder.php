<?php

use Illuminate\Database\Seeder;

class UserRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 200; $i++) {
            DB::table('role_user')->insert(
                [
                    [
                        'role_id' => App\Models\Role::all()->random()->id,
                        'user_id' =>  App\User::all()->random()->id,
                    ],

                ]);
        }        //
    }
}
