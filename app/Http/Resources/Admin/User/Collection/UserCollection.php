<?php

namespace App\Http\Resources\Admin\User\Collection;

use Illuminate\Http\Resources\Json\ResourceCollection;

class UserCollection extends ResourceCollection
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'=> (string) $this->name,
            'slug'=> (string) $this->slug,
            'email'=> (string) $this->email,
            'link'=>[
                'self'=>Route('admin.users.show',$this->id),
            ],
        ];
    }
}
