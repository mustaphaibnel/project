<?php

namespace App\Http\Controllers\api\v1\Admin;


use App\Http\Requests\MarqueRequest;
use App\Http\Resources\Admin\Marque\Collection\MarqueCollection;
use App\Http\Resources\Admin\Agence\Collection\AgenceCollection;
use App\Http\Resources\Admin\Marque\Resource\MarqueResource;
use App\Http\Resources\Admin\Modele\Collection\ModeleCollection;
use App\Http\Resources\Admin\Vehicule\Collection\VehiculeCollection;
use App\Models\Marque;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


class MarqueController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $marques = Marque::paginate();
        //return $marques;
        return MarqueCollection::collection($marques);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(MarqueRequest $request)
    {
        $marque=new Marque();
        $marque=$request;
        $marque->save;
        return Response()->json('ok',200);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Marque  $marque
     * @return \Illuminate\Http\Response
     */
    public function show($marque)
    {

        $marque =Marque::find($marque);

        return new  MarqueResource($marque);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Marque  $marque
     * @return \Illuminate\Http\Response
     */
    public function edit(Marque $marque)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Marque  $marque
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Marque $marque)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Marque  $marque
     * @return \Illuminate\Http\Response
     */
    public function destroy(Marque $marque)
    {
        //
    }
    public function modeles($marque_id)
    {

        $marque =Marque::find($marque_id);
        $modeles= $marque->modeles()->paginate();
        return ModeleCollection::collection($modeles);
    }
    public function vehicules($marque_id)
    {

        $marque =Marque::find($marque_id);
        $vehicules= $marque->vehicules()->paginate();
        return VehiculeCollection::collection($vehicules);
    }
    public function agences($marque_id)
    {

        $marque =Marque::find($marque_id);
        $agences= $marque->vehicules()->get()->pluck('agence');
        return AgenceCollection::collection($agences);
    }
}
