<?php

namespace App\Http\Middleware\Custom;


use Closure;

class UserCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->isUser()){
            return $next($request);
        }
        else{

            return response([['errors'=>"Unauthorized action.",'details'=>["type"=> 'UnauthorizeScope',"message"=> 'scope inaccessible pour votre type de compte']]], 403);

        }
    }
}
