<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Tariff::class, function (Faker $faker) {
    return [
'jour_debut'=>22,
'jour_final'=>365,
'prix'=>200,
'prix_promotionnel'=>190,
'vehicule_id'=>\App\Models\Vehicule::all()->random()->id,
    ];
});
