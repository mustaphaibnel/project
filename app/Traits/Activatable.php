<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

trait Activatable
{
    /**
     * Scope a query to only include activated models.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeActivated(Builder $query)
    {
        return $query->where('activated_at', '<=', Carbon::now())->whereNotNull('activated_at');
    }

    /**
     * Scope a query to only include unactivated models.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUnactivated(Builder $query)
    {
        return $query->where('activated_at', '>', Carbon::now())->orWhereNull('activated_at');
    }

    /**
     * @return bool
     */
    public function isActivated()
    {
        if (is_null($this->activated_at)) {
            return false;
        }

        return $this->activated_at->lte(Carbon::now());
    }

    /**
     * @return bool
     */
    public function isUnactivated()
    {
        return !$this->isActivated();
    }

    /**
     * @return bool
     */
    public function activate()
    {
        return $this->update([
            'activated_at' => Carbon::now()->toDateTimeString(),
        ]);
    }

    /**
     * @return bool
     */
    public function unactivate()
    {
        return $this->update([
            'activated_at' => null,
        ]);
    }

    /**
     * @param  mixed $value
     * @return \Carbon\Carbon
     */
    public function getActivatedAtAttribute($value)
    {
        if (is_null($value)) {
            return $value;
        }

        return $this->asDateTime($value);
    }
}