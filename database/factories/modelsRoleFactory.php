<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Role::class, function (Faker $faker) {
    return [
        [
            'name'=>'client',
            'slug'=>'client',
            'description'=>'client',
        ],        [
            'name'=>'admin',
            'slug'=>'admin',
            'description'=>'Super Admin',
        ],
        [
            'name'=>'chef',
            'slug'=>'chef',
            'description'=>'chef d\'agence',
        ],
        [
            'name'=>'employee',
            'slug'=>'employee',
            'description'=>'employee',
        ],
        [
            'name'=>'responsable',
            'slug'=>'responsable',
            'description'=>'responsable',
        ],
        [
            'name'=>'guest',
            'slug'=>'guest',
            'description'=>'guest',
        ],
    ];
});
