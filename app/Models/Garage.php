<?php

namespace App\Models;

use App\Scopes\GarageScope;
use App\User;

class Garage extends AgenceGarage
{


    public static function boot()
    {
        parent::boot();
        static::addGlobalScope(new GarageScope);
    }
    public function agence(){
        return $this->belongsTo(Agence::class,'agence_id');
    }
    public function vehicules(){
        return $this->hasMany(Vehicule::class);
    }
    public function locations()
    {
        return $this->hasManyThrough(Location::class,Vehicule::class);
    }
    public function reservations()
    {
        return $this->hasManyThrough(Reservation::class,Vehicule::class);
    }

    public function users()
    {
        return $this->morphedByMany(User::class, 'garageable');
    }

    /**
     * Get all of the videos that are assigned this tag.
     */
    public function clients()
    {
        return $this->morphedByMany(Client::class, 'garageable');
    }
}
