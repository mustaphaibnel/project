<?php

namespace App\Http\Resources\Client\Profil\Collection;

use Illuminate\Http\Resources\Json\JsonResource;

class ProfilCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
