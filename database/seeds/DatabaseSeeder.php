<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(RoleTableSeeder::class);

        /*
        $this->call(RoleTableSeeder::class);
        $this->call(PermissionRoleTableSeeder::class);
        factory(\App\User::class,20)->create();
        $this->call(UserRoleTableSeeder::class);
        factory(\App\Models\Marque::class,50)->create();
        factory(\App\Models\Modele::class,500)->create();
        factory(\App\Models\AgenceGarage::class,50)->create();
        /*factory(\App\Models\Agence::class,50)->create();
        factory(\App\Models\Garage::class,500)->create();
        factory(\App\Models\Client::class,500)->create();
        factory(\App\Models\Vehicule::class,200)->create();

        factory(\App\Models\Reservation::class,200)->create();
        factory(\App\Models\Tariff::class,200)->create();
        factory(\App\Models\Location::class,400)->create();
       /* factory(\App\Models\Profilable::class,400)->create();
        //factory(\App\Models\Profilable::class,400)->create();*/
    }
}
