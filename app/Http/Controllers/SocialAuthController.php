<?php
namespace App\Http\Controllers;

use App\SocialAccount;
use Illuminate\Http\RedirectResponse;
use Socialite;
use App\User;
use Tymon\JWTAuth\Facades\JWTAuth;

class SocialAuthController extends Controller
{
    public function redirect($service)
    {
        return Socialite::driver($service)->redirect();
    }

    public function callback($service)
    {
        $user =Socialite::driver($service)->user();
        //dd($user);
        $user = $this->findOrCreateUser($user,$service);

        $token =JWTAuth::fromUser($user);
         //return $this->respondWithToken($token);
       // dd($getparam);
       /* $myurl='http://localhost/test.html';
        return response()->redirectTo($myurl)
            ->header('mykey', 'ok my test');
*/
       return redirect()->away('http://localhost/test.php?token='.$token,302,$headers=['token'=>$token],true)->with(['test'=>'my test']);
    //dd($test);

    }
    public function findOrCreateUser($user,$service)
    {
        $account = SocialAccount::whereProvider($service)
            ->whereProviderUserId($user->getId())
            ->first();

        if ($account) {
            return $account->user;
        } else {

            $account = new SocialAccount([
                'provider_user_id' => $user->getId(),
                'provider' => $service,
            ]);
            $finduser = User::whereEmail($user->getEmail())->first();



            if (!$finduser) {

                $user = User::create([
                    'email' => $user->getEmail(),
                    'name' => $user->getName(),
                    'password'    => bcrypt('secret'),
                    'role_id'    => 1,
                    'is_active'    => 1,
                ]);
            }else{
                $user= $finduser;
            }

            $account->user()->associate($user);
            $account->save();

            return $user;

        }

    }
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth('api')->factory()->getTTL() * 60
        ]);
    }
}