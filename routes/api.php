<?php

use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Http\Resources\Api\V1\UserResource;
use App\User;


Route::get('steef',function (Request $request){
    $app = app();
    $routes = $app->routes->getRoutes();
    return Response()->json($routes);
});

Route::group([
    'middleware' => 'api',
    'prefix' => 'auth',
    'namespace'=>'api\v1\auth'

], function () {

    Route::post('login', 'AuthJwtController@login');
    Route::post('logout', 'AuthJwtController@logout');
    Route::post('refresh', 'AuthJwtController@refresh');
    Route::post('me', 'AuthJwtController@me')->middleware('auth:api');

});


//route group for Admin
Route::group(['prefix' => 'v1/admin', 'namespace' => 'api\v1\Admin','middleware'=>['auth:api','throttle:40,1','usercheck','activated','verified','permissioncheck']], function()
{
    //roles
    Route::get('roles', 'RoleController@index')->name('admin.roles.index');
    Route::post('roles', 'RoleController@store')->name('admin.roles.store');
    Route::get('roles/{id}', 'RoleController@show')->name('admin.roles.show');
    Route::put('roles/{id}', 'RoleController@update')->name('admin.roles.update');
    Route::delete('roles/{id}', 'RoleController@destroy')->name('admin.roles.destroy');
    Route::get('roles/{id}/permissions', 'RoleController@permissions')->name('admin.roles.permissions');
    Route::post('roles/{id}/attachrole', 'RoleController@attachRole')->name('admin.roles.attachRole');
    Route::post('roles/{id}/dettachrole', 'RoleController@dettachRole')->name('admin.roles.dettachRole');

    //permissions
    Route::get('permissions', 'PermissionController@index')->name('admin.permissions.index');
    Route::post('permissions', 'PermissionController@store')->name('admin.permissions.store');
    Route::get('permissions/{id}', 'PermissionController@show')->name('admin.permissions.show');
    Route::put('permissions/{id}', 'PermissionController@update')->name('admin.permissions.update');
    Route::delete('permissions/{id}', 'PermissionController@destroy')->name('admin.permissions.destroy');
    Route::get('permissions/{id}/roles', 'PermissionController@roles')->name('admin.permissions.roles');
    Route::post('permissions/{id}/attachpermission', 'PermissionController@attachPermission')->name('admin.permissions.attachPermission');
    Route::post('permissions/{id}/dettachpermission', 'PermissionController@dettachPermission')->name('admin.permissions.dettachPermission');

    //users

    Route::get('users', 'UserController@index')->name('admin.users.index');
    Route::post('users', 'UserController@store')->name('admin.users.store');
    Route::get('users/{id}', 'UserController@show')->name('admin.users.show');
    Route::put('users/{id}', 'UserController@update')->name('admin.users.update');
    Route::delete('users/{id}', 'UserController@destroy')->name('admin.users.destroy');


    Route::post('users/{id}/attachrole', 'UserController@attachRole')->name('admin.users.attachRole');
    Route::post('users/{id}/dettachrole', 'UserController@dettachRole')->name('admin.users.dettachRole');


    Route::post('users/{id}/activate', 'UserController@activate')->name('admin.users.activate');
    Route::post('users/{id}/desactivate', 'UserController@desactivate')->name('admin.users.desactivate');


    Route::get('users/{id}/roles', 'UserController@roles')->name('admin.users.roles');
    Route::get('users/{id}/garages', 'UserController@garages')->name('admin.users.garages');
    Route::get('users/{id}/agences', 'UserController@agences')->name('admin.users.agences');
    Route::get('users/{id}/vehicules', 'UserController@vehicules')->name('admin.users.vehicules');
    Route::get('users/{id}/reservations', 'UserController@reservations')->name('admin.users.reservations');
    Route::get('users/{id}/locations', 'UserController@locations')->name('admin.users.locations');
    Route::get('users/{id}/clients', 'UserController@clients')->name('admin.users.clients');


    //clients
    Route::get('clients', 'ClientController@index')->name('admin.clients.index');
    Route::post('clients', 'ClientController@store')->name('admin.clients.store');
    Route::get('clients/{id}', 'ClientController@show')->name('admin.clients.show');
    Route::put('clients/{id}', 'ClientController@update')->name('admin.clients.update');
    Route::delete('clients/{id}', 'ClientController@destroy')->name('admin.clients.destroy');


    Route::post('clients/{id}/attachrole', 'ClientController@attachRole')->name('admin.clients.attachRole');
    Route::post('clients/{id}/dettachrole', 'ClientController@dettachRole')->name('admin.clients.dettachRole');


    Route::post('clients/{id}/activate', 'ClientController@activate')->name('admin.clients.activate');
    Route::post('clients/{id}/desactivate', 'ClientController@desactivate')->name('admin.clients.desactivate');


    Route::get('clients/{id}/roles', 'ClientController@roles')->name('admin.clients.roles');
    Route::get('clients/{id}/garages', 'ClientController@garages')->name('admin.clients.garages');
    Route::get('clients/{id}/agences', 'ClientController@agences')->name('admin.clients.agences');
    Route::get('clients/{id}/vehicules', 'ClientController@vehicules')->name('admin.clients.vehicules');
    Route::get('clients/{id}/reservations', 'ClientController@reservations')->name('admin.clients.reservations');
    Route::get('clients/{id}/locations', 'ClientController@locations')->name('admin.clients.locations');
    Route::get('clients/{id}/users', 'ClientController@users')->name('admin.clients.users');

    //marques

    Route::get('marques', 'MarqueController@index')->name('admin.marques.index');
    Route::post('marques', 'MarqueController@store')->name('admin.marques.store');
    Route::get('marques/{id}', 'MarqueController@show')->name('admin.marques.show');
    Route::put('marques/{id}', 'MarqueController@update')->name('admin.marques.update');
    Route::delete('marques/{id}', 'MarqueController@destroy')->name('admin.marques.destroy');

    Route::get('marques/{id}/modeles', 'MarqueController@modeles')->name('admin.marques.modeles');
    Route::get('marques/{id}/garages', 'MarqueController@garages')->name('admin.marques.garages');
    Route::get('marques/{id}/agences', 'MarqueController@agences')->name('admin.marques.agences');
    Route::get('marques/{id}/vehicules', 'MarqueController@vehicules')->name('admin.marques.vehicules');
    Route::get('marques/{id}/reservations', 'MarqueController@reservations')->name('admin.marques.reservations');
    Route::get('marques/{id}/locations', 'MarqueController@locations')->name('admin.marques.locations');

    //modeles

    Route::get('modeles', 'ModeleController@index')->name('admin.modeles.index');
    Route::post('modeles', 'ModeleController@store')->name('admin.modeles.store');
    Route::get('modeles/{id}', 'ModeleController@show')->name('admin.modeles.show');
    Route::put('modeles/{id}', 'ModeleController@update')->name('admin.modeles.update');
    Route::delete('modeles/{id}', 'ModeleController@destroy')->name('admin.modeles.destroy');

    Route::get('modeles/{id}/garages', 'ModeleController@garages')->name('admin.modeles.garages');
    Route::get('modeles/{id}/agences', 'ModeleController@agences')->name('admin.modeles.agences');
    Route::get('modeles/{id}/vehicules', 'ModeleController@vehicules')->name('admin.modeles.vehicules');
    Route::get('modeles/{id}/reservations', 'ModeleController@reservations')->name('admin.modeles.reservations');
    Route::get('modeles/{id}/locations', 'ModeleController@locations')->name('admin.modeles.locations');


    //agences
    Route::get('agences', 'AgenceController@index')->name('admin.agences.index');
    Route::post('agences', 'AgenceController@store')->name('admin.agences.store');
    Route::get('agences/{id}', 'AgenceController@show')->name('admin.agences.show');
    Route::put('agences/{id}', 'AgenceController@update')->name('admin.agences.update');
    Route::delete('agences/{id}', 'AgenceController@destroy')->name('admin.agences.destroy');


    Route::post('agences/{id}/activate', 'AgenceController@activate')->name('admin.agences.activate');
    Route::post('agences/{id}/desactivate', 'AgenceController@desactivate')->name('admin.agences.desactivate');

    Route::get('agences/{id}/marques', 'AgenceController@marques')->name('admin.agences.marques');
    Route::get('agences/{id}/modeles', 'AgenceController@modeles')->name('admin.agences.modeles');
    Route::get('agences/{id}/garages', 'AgenceController@garages')->name('admin.agences.garages');
    Route::get('agences/{id}/vehicules', 'AgenceController@vehicules')->name('admin.agences.vehicules');
    Route::get('agences/{id}/reservations', 'AgenceController@reservations')->name('admin.agences.reservations');
    Route::get('agences/{id}/locations', 'AgenceController@locations')->name('admin.agences.locations');
    Route::get('agences/{id}/clients', 'AgenceController@clients')->name('admin.agences.clients');
    Route::get('agences/{id}/users', 'AgenceController@users')->name('admin.agences.users');

    //garages

    Route::get('garages', 'GarageController@index')->name('admin.garages.index');
    Route::post('garages', 'GarageController@store')->name('admin.garages.store');
    Route::get('garages/{id}', 'GarageController@show')->name('admin.garages.show');
    Route::put('garages/{id}', 'GarageController@update')->name('admin.garages.update');
    Route::delete('garages/{id}', 'GarageController@destroy')->name('admin.garages.destroy');


    Route::post('garages/{id}/activate', 'GarageController@activate')->name('admin.garages.activate');
    Route::post('garages/{id}/desactivate', 'GarageController@desactivate')->name('admin.garages.desactivate');


    Route::post('garages/{id}/attachuser', 'GarageController@attachUser')->name('admin.garages.attachUser');
    Route::post('garages/{id}/dettachuser', 'GarageController@dettachUser')->name('admin.garages.dettachUser');

    Route::get('garages/{id}/marques', 'GarageController@marques')->name('admin.garages.marques');
    Route::get('garages/{id}/modeles', 'GarageController@modeles')->name('admin.garages.modeles');
    Route::get('garages/{id}/vehicules', 'GarageController@vehicules')->name('admin.garages.vehicules');
    Route::get('garages/{id}/reservations', 'GarageController@reservations')->name('admin.garages.reservations');
    Route::get('garages/{id}/locations', 'GarageController@locations')->name('admin.garages.locations');
    Route::get('garages/{id}/clients', 'GarageController@clients')->name('admin.garages.clients');
    Route::get('garages/{id}/users', 'GarageController@users')->name('admin.garages.users');



    //vehicules
    Route::get('vehicules', 'VehiculeController@index')->name('admin.vehicules.index');
    Route::post('vehicules', 'VehiculeController@store')->name('admin.vehicules.store');
    Route::get('vehicules/{id}', 'VehiculeController@show')->name('admin.vehicules.show');
    Route::put('vehicules/{id}', 'VehiculeController@update')->name('admin.vehicules.update');
    Route::delete('vehicules/{id}', 'VehiculeController@destroy')->name('admin.vehicules.destroy');


    Route::get('vehicules/{id}/reservations', 'VehiculeController@reservations')->name('admin.vehicules.reservations');
    Route::get('vehicules/{id}/locations', 'VehiculeController@locations')->name('admin.vehicules.locations');
    Route::get('vehicules/{id}/clients', 'VehiculeController@clients')->name('admin.vehicules.clients');
    Route::get('vehicules/{id}/users', 'VehiculeController@users')->name('admin.vehicules.users');

    //tariffs
    Route::get('tariffs', 'TariffController@index')->name('admin.tariffs.index');
    Route::post('tariffs', 'TariffController@store')->name('admin.tariffs.store');
    Route::get('tariffs/{id}', 'TariffController@show')->name('admin.tariffs.show');
    Route::put('tariffs/{id}', 'TariffController@update')->name('admin.tariffs.update');
    Route::delete('tariffs/{id}', 'TariffController@destroy')->name('admin.tariffs.destroy');

    //reservations
    Route::get('reservations', 'ReservationController@index')->name('admin.reservations.index');
    Route::post('reservations', 'ReservationController@store')->name('admin.reservations.store');
    Route::get('reservations/{id}', 'ReservationController@show')->name('admin.reservations.show');
    Route::put('reservations/{id}', 'ReservationController@update')->name('admin.reservations.update');
    Route::delete('reservations/{id}', 'ReservationController@destroy')->name('admin.reservations.destroy');


    Route::post('reservations/{id}/validate', 'ReservationController@validate')->name('admin.reservations.validate');
    Route::post('reservations/{id}/ignore', 'ReservationController@ignore')->name('admin.reservations.ignore');
    Route::post('reservations/{id}/tolocation', 'ReservationController@toLocation')->name('admin.reservations.toLocation');


    Route::get('reservations/{id}/clients', 'ReservationController@clients')->name('admin.reservations.clients');
    Route::get('reservations/{id}/users', 'ReservationController@users')->name('admin.reservations.users');


    //locations
    Route::get('locations', 'LocationController@index')->name('admin.locations.index');
    Route::post('locations', 'LocationController@store')->name('admin.locations.store');
    Route::get('locations/{id}', 'LocationController@show')->name('admin.locations.show');
    Route::put('locations/{id}', 'LocationController@update')->name('admin.locations.update');
    Route::delete('locations/{id}', 'LocationController@destroy')->name('admin.locations.destroy');


    Route::post('locations/{id}/validate', 'LocationController@validate')->name('admin.locations.validate');
    Route::post('locations/{id}/ignore', 'LocationController@ignore')->name('admin.locations.ignore');
    Route::post('locations/{id}/tolocation', 'LocationController@toLocation')->name('admin.locations.toLocation');


    Route::get('locations/{id}/clients', 'LocationController@clients')->name('admin.locations.clients');
    Route::get('locations/{id}/users', 'LocationController@users')->name('admin.locations.users');






});


//route group for Client
Route::group(['prefix' => 'v1/client', 'namespace' => 'api\v1\Client'], function()
{



    //clients

    Route::get('clients/{id}', 'ClientController@show')->name('client.clients.show');
    Route::get('clients/{id}/garages', 'ClientController@garages')->name('client.clients.garages');
    Route::get('clients/{id}/agences', 'ClientController@agences')->name('client.clients.agences');
    Route::get('clients/{id}/vehicules', 'ClientController@vehicules')->name('client.clients.vehicules');
    Route::get('clients/{id}/reservations', 'ClientController@reservations')->name('client.clients.reservations');
    Route::get('clients/{id}/locations', 'ClientController@locations')->name('client.clients.locations');

    //marques

    Route::get('marques', 'MarqueController@index')->name('client.marques.index');
    Route::get('marques/{id}', 'MarqueController@show')->name('client.marques.show');

    Route::get('marques/{id}/modeles', 'MarqueController@modeles')->name('client.marques.modeles');
    Route::get('marques/{id}/garages', 'MarqueController@garages')->name('client.marques.garages');
    Route::get('marques/{id}/agences', 'MarqueController@agences')->name('client.marques.agences');

    Route::get('marques/{id}/reservations', 'MarqueController@reservations')->name('client.marques.reservations');
    Route::get('marques/{id}/locations', 'MarqueController@locations')->name('client.marques.locations');

    //modeles

    Route::get('modeles', 'ModeleController@index')->name('client.modeles.index');
    Route::get('modeles/{id}', 'ModeleController@show')->name('client.modeles.show');

    Route::get('modeles/{id}/garages', 'ModeleController@garages')->name('client.modeles.garages');
    Route::get('modeles/{id}/agences', 'ModeleController@agences')->name('client.modeles.agences');
    Route::get('modeles/{id}/vehicules', 'ModeleController@vehicules')->name('client.modeles.vehicules');
    Route::get('modeles/{id}/reservations', 'ModeleController@reservations')->name('client.modeles.reservations');
    Route::get('modeles/{id}/locations', 'ModeleController@locations')->name('client.modeles.locations');


    //agences
    Route::get('agences', 'AgenceController@index')->name('client.agences.index');
    Route::get('agences/{id}', 'AgenceController@show')->name('client.agences.show');

    Route::get('agences/{id}/marques', 'AgenceController@marques')->name('client.agences.marques');
    Route::get('agences/{id}/modeles', 'AgenceController@modeles')->name('client.agences.modeles');
    Route::get('agences/{id}/garages', 'AgenceController@garages')->name('client.agences.garages');
    Route::get('agences/{id}/vehicules', 'AgenceController@vehicules')->name('client.agences.vehicules');
    Route::get('agences/{id}/reservations', 'AgenceController@reservations')->name('client.agences.reservations');
    Route::get('agences/{id}/locations', 'AgenceController@locations')->name('client.agences.locations');

    //garages

    Route::get('garages', 'GarageController@index')->name('client.garages.index');
    Route::get('garages/{id}', 'GarageController@show')->name('client.garages.show');

    Route::get('garages/{id}/marques', 'GarageController@marques')->name('client.garages.marques');
    Route::get('garages/{id}/modeles', 'GarageController@modeles')->name('client.garages.modeles');
    Route::get('garages/{id}/vehicules', 'GarageController@vehicules')->name('client.garages.vehicules');
    Route::get('garages/{id}/reservations', 'GarageController@reservations')->name('client.garages.reservations');
    Route::get('garages/{id}/locations', 'GarageController@locations')->name('client.garages.locations');



    //vehicules
    Route::get('vehicules', 'VehiculeController@index')->name('client.vehicules.index');
    Route::get('vehicules/{id}', 'VehiculeController@show')->name('client.vehicules.show');


    Route::get('vehicules/{id}/reservations', 'VehiculeController@reservations')->name('client.vehicules.reservations');
    Route::get('vehicules/{id}/locations', 'VehiculeController@locations')->name('client.vehicules.locations');

    //tariffs
    Route::get('tariffs', 'TariffController@index')->name('client.tariffs.index');

    Route::get('tariffs/{id}', 'TariffController@show')->name('client.tariffs.show');

    //reservations
    Route::get('reservations', 'ReservationController@index')->name('client.reservations.index');
    Route::post('reservations', 'ReservationController@store')->name('client.reservations.store');
    Route::get('reservations/{id}', 'ReservationController@show')->name('client.reservations.show');
    Route::put('reservations/{id}', 'ReservationController@update')->name('client.reservations.update');
    Route::delete('reservations/{id}', 'ReservationController@destroy')->name('client.reservations.destroy');

    Route::post('reservations/{id}/ignore', 'ReservationController@ignore')->name('client.reservations.ignore');
    Route::post('reservations/{id}/tolocation', 'ReservationController@toLocation')->name('client.reservations.toLocation');


    //locations
    Route::get('locations', 'LocationController@index')->name('client.locations.index');
    Route::get('locations/{id}', 'LocationController@show')->name('client.locations.show');


});


//route group for Guest
Route::group(['prefix' => 'v1/guest', 'namespace' => 'api\v1\Guest'], function()
{




    //marques

    Route::get('marques', 'MarqueController@index')->name('guest.marques.index');
    Route::get('marques/{id}', 'MarqueController@show')->name('guest.marques.show');

    Route::get('marques/{id}/modeles', 'MarqueController@modeles')->name('guest.marques.modeles');
    Route::get('marques/{id}/garages', 'MarqueController@garages')->name('guest.marques.garages');
    Route::get('marques/{id}/agences', 'MarqueController@agences')->name('guest.marques.agences');


    //modeles

    Route::get('modeles', 'ModeleController@index')->name('guest.modeles.index');
    Route::get('modeles/{id}', 'ModeleController@show')->name('guest.modeles.show');

    Route::get('modeles/{id}/garages', 'ModeleController@garages')->name('guest.modeles.garages');
    Route::get('modeles/{id}/agences', 'ModeleController@agences')->name('guest.modeles.agences');
    Route::get('modeles/{id}/vehicules', 'ModeleController@vehicules')->name('guest.modeles.vehicules');



    //agences
    Route::get('agences', 'AgenceController@index')->name('guest.agences.index');
    Route::get('agences/{id}', 'AgenceController@show')->name('guest.agences.show');

    Route::get('agences/{id}/marques', 'AgenceController@marques')->name('guest.agences.marques');
    Route::get('agences/{id}/modeles', 'AgenceController@modeles')->name('guest.agences.modeles');
    Route::get('agences/{id}/garages', 'AgenceController@garages')->name('guest.agences.garages');
    Route::get('agences/{id}/vehicules', 'AgenceController@vehicules')->name('guest.agences.vehicules');


    //garages

    Route::get('garages', 'GarageController@index')->name('guest.garages.index');
    Route::get('garages/{id}', 'GarageController@show')->name('guest.garages.show');

    Route::get('garages/{id}/marques', 'GarageController@marques')->name('guest.garages.marques');
    Route::get('garages/{id}/modeles', 'GarageController@modeles')->name('guest.garages.modeles');
    Route::get('garages/{id}/vehicules', 'GarageController@vehicules')->name('guest.garages.vehicules');



    //vehicules
    Route::get('vehicules', 'VehiculeController@index')->name('guest.vehicules.index');
    Route::get('vehicules/{id}', 'VehiculeController@show')->name('guest.vehicules.show');



    //tariffs
    Route::get('tariffs', 'TariffController@index')->name('guest.tariffs.index');

    Route::get('tariffs/{id}', 'TariffController@show')->name('guest.tariffs.show');




});
/*
//route group for Documentaion
Route::group(['prefix' => 'v1/documentation', 'namespace' => 'api\v1\Documentaion'], function()
{
    

     //groups
     Route::get('groups', 'GroupController@index')->name('documentation.groups.index');
     Route::post('groups', 'GroupController@store')->name('documentation.groups.store');
     Route::get('groups/{id}', 'GroupController@show')->name('documentation.groups.show');
     Route::put('groups/{id}', 'GroupController@update')->name('documentation.groups.update');
     Route::delete('groups/{id}', 'GroupController@destroy')->name('documentation.groups.destroy');

     Route::get('groups/{id}/urls', 'GroupController@urls')->name('documentation.groups.urls');



});
/*
//personal Auth/Passport
Route::group(['middleware' => 'api', 'throttle:rate_limit,1'], function(){
    Route::post('login', 'api\v1\auth\PassportController@login')->name('api.login');
    Route::post('register', 'api\v1\auth\PassportController@register')->name('api.register');
});


Route::group(['middleware' => 'auth:api', 'throttle:2,1'], function(){
    Route::post('userdetails', 'api\v1\auth\PassportController@getDetails')->name('api.user');;
    Route::post('logout','api\v1\auth\PassportController@logoutApi')->name('api.logout');;
});



Route::middleware('auth:api')->get('/user', function (Request $request) {
    $id= $request->user()->id;
    return new UserResource(User::find($id));
})->name('user.get');
route group for admins
Route::group(
    [
        'prefix' => 'v1/admin', 'namespace' => 'api\v1\Admin',
        'middleware'=> ['auth:api','permissioncheck','isactiveuser','isuser']],
    function() {
    // Controllers Within The "App\Http\Controllers\Admin\User" Namespace
    //Route::get('garages','GarageController@index');
    Route::apiResource('garages', 'GarageController');
    Route::apiResource('roles', 'RoleController');
    Route::apiResource('agences', 'AgenceController');
    Route::get('/firstGarage','GarageController@firstGarage')->name('garages.first');
});


//route group for clients
Route::group(
    [
        'prefix' => 'v1/client', 'namespace' => 'api\v1\Client',
        'middleware'=> ['auth:api','permissioncheck','isactiveuser','isclient']],
    function() {
        // Controllers Within The "App\Http\Controllers\Admin\User" Namespace
        //Route::get('garages','GarageController@index');
        Route::apiResource('garages', 'GarageController');
        Route::apiResource('roles', 'RoleController');
        Route::apiResource('agences', 'AgenceController');
        Route::get('/firstGarage','GarageController@firstGarage')->name('garages.first');
    });

/*Route::apiResource('agences.garages','GarageController')->parameters([
    'agences'=>'id_agence',
    'garages'=>'id_garage',

]);


Route::group(['prefix' => 'v1', 'namespace' => 'api\v1'], function()
{
    // Controllers Within The "App\Http\Controllers\Admin\User" Namespace
    //Route::get('garages','GarageController@index');

    Route::apiResource('garages', 'GarageController')->middleware('auth:api');


});
*/


/*
Route::group(['prefix' => 'v1', 'namespace' => 'api\v1'],['middleware'=>'api'], function()
{
    // Controllers Within The "App\Http\Controllers\Admin\User" Namespace
    //Route::get('garages','GarageController@index');



});
//Route::apiResource('garages','GarageController')->m;

/*
Route::group(['prefix' => 'api/v1', 'namespace' => 'api\v1'],['middleware'=>'auth:api'], function()
{
    // Controllers Within The "App\Http\Controllers\Admin\User" Namespace



});
*/