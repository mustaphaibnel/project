<?php

namespace App\Http\Controllers\api\v1\Admin;


use App\models\Profilable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfilController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\models\Profilable  $profil
     * @return \Illuminate\Http\Response
     */
    public function show(Profilable $profil)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\models\Profilable  $profil
     * @return \Illuminate\Http\Response
     */
    public function edit(Profilable $profil)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\models\Profilable  $profil
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Profilable $profil)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\models\Profilable  $profil
     * @return \Illuminate\Http\Response
     */
    public function destroy(Profilable $profil)
    {
        //
    }
}
