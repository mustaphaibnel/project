<?php

namespace App\Http\Controllers\api\v1\Admin;


use App\Http\Resources\Admin\Location\Collection\LocationCollection;
use App\Http\Resources\Admin\Reservation\Collection\ReservationCollection;
use App\Http\Resources\Admin\Vehicule\Collection\VehiculeCollection;
use App\Http\Resources\Admin\Vehicule\Resource\VehiculeResource;
use App\Models\Vehicule;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VehiculeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    private $user=null;

    public function __construct()
    {
        $this->user=auth('api')->user();
    }

    public function index()
    {
        $vehicules= $this->user->vehicules();
        return  VehiculeCollection::collection($vehicules);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Vehicule  $vehicule
     * @return \Illuminate\Http\Response
     */
    public function show($vehiculeId)
    {
        $vehicule=Vehicule::find($vehiculeId);

        if ($this->user->can('show', $vehicule)) {

            return new VehiculeResource($vehicule);

        }
        else{
            return response('Unauthorized action.', 403);
        }

    }
    public function locations($vehiculeId)
    {
        $vehicule=Vehicule::find($vehiculeId);
        if ($this->user->can('locations', $vehicule)) {

            $locations= $vehicule->locations()->paginate();
            return  LocationCollection::collection($locations);
        }
        else{
            return response('Unauthorized action.', 403);
        }

    }
    public function reservations($vehiculeId)
    {
        $vehicule=Vehicule::find($vehiculeId);
        if ($this->user->can('reservations', $vehicule)) {

            $reservations= $vehicule->reservations()->paginate();
            return  ReservationCollection::collection($reservations);
        }
        else{
            return response('Unauthorized action.', 403);
        }

    }


    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Vehicule  $vehicule
     * @return \Illuminate\Http\Response
     */
    public function edit(Vehicule $vehicule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Vehicule  $vehicule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Vehicule $vehicule)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Vehicule  $vehicule
     * @return \Illuminate\Http\Response
     */
    public function destroy(Vehicule $vehicule)
    {
        //
    }
}
