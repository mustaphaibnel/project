<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGarageablesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('garageables', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('garage_id');
            $table->unsignedInteger('garageable_id');
            $table->string('garageable_type');
            $table->Date('date_nissance')->nullable();
            $table->string('lieu_nissance')->nullable();
            $table->string('img_cin')->nullable();
            $table->string('img_permis')->nullable();
            $table->string('telephone')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('garageables');
    }
}
