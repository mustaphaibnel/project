<?php

namespace App\Http\Resources\Client\Agence\Collection;

use Illuminate\Http\Resources\Json\JsonResource;

class AgenceCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'slug'=>$this->slug,
            'name'=>$this->name,
        ];
    }
}
