<?php

namespace App\Http\Resources\Admin\Permission\Collection;

use Illuminate\Http\Resources\Json\JsonResource;

class PermissionCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
