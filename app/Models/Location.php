<?php

namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;

class Location extends Model
{

    public function user(){
        return $this->belongsTo(User::class);
    }
    public function client(){
        return $this->belongsTo(Client::class);
    }
    public function vehicule(){
        return $this->belongsTo(Vehicule::class);
    }
    public function garage(){
        return $this->vehicule->garage;
    }
    public function agence(){
        return $this->vehicule->garage->agence;

    }


}
