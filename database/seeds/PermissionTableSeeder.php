<?php

use Illuminate\Database\Seeder;

class PermissionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert(
            [
                [
                    'name'=>'admin.agences.index',
                    'slug'=>'admin.agences.index',
                    'description'=>'Index des agences',
                ],
                [
                    'name'=>'admin.agences.store',
                    'slug'=>'admin.agences.store',
                    'description'=>'stockage des agences',
                ],
                [
                    'name'=>'admin.agences.show',
                    'slug'=>'admin.agences.show',
                    'description'=>'show des agences',
                ],
                [
                    'name'=>'admin.agences.update',
                    'slug'=>'admin.agences.update',
                    'description'=>'update des agences',
                ],
                [
                    'name'=>'admin.agences.destroy',
                    'slug'=>'admin.agences.destroy',
                    'description'=>'destroy des agences',
                ],
                [
                    'name'=>'admin.agences.activate',
                    'slug'=>'admin.agences.activate',
                    'description'=>'activate des agences',
                ],
                [
                    'name'=>'admin.agences.clients',
                    'slug'=>'admin.agences.clients',
                    'description'=>'clients des agences',
                ],
                [
                    'name'=>'admin.agences.desactivate',
                    'slug'=>'admin.agences.desactivate',
                    'description'=>'desactivate des agences',
                ],
                [
                    'name'=>'admin.agences.employees',
                    'slug'=>'admin.agences.employees',
                    'description'=>'employees des agences',
                ],
                [
                    'name'=>'admin.agences.garages',
                    'slug'=>'admin.agences.garages',
                    'description'=>'garages des agences',
                ],
                [
                    'name'=>'admin.agences.locations',
                    'slug'=>'admin.agences.locations',
                    'description'=>'locations des voitures',
                ],
                [
                    'name'=>'admin.agences.marques',
                    'slug'=>'admin.agences.marques',
                    'description'=>'marques des voitures',
                ],
                [
                    'name'=>'admin.agences.modeles',
                    'slug'=>'admin.agences.modeles',
                    'description'=>'modeles des voitures',
                ],
                [
                    'name'=>'admin.agences.reservations',
                    'slug'=>'admin.agences.reservations',
                    'description'=>'reservations des voitures',
                ],
                [
                    'name'=>'admin.agences.vehicules',
                    'slug'=>'admin.agences.vehicules',
                    'description'=>'vehicules des agences',
                ],
                [
                    'name'=>'admin.clients.store',
                    'slug'=>'admin.clients.store',
                    'description'=>'stockage des clients',
                ],
                [
                    'name'=>'admin.clients.index',
                    'slug'=>'admin.clients.index',
                    'description'=>'index des clients',
                ],
                [
                    'name'=>'admin.clients.destroy',
                    'slug'=>'admin.clients.destroy',
                    'description'=>'destroy des clients',
                ],
                [
                    'name'=>'admin.clients.update',
                    'slug'=>'admin.clients.update',
                    'description'=>'update des clients',
                ],
                [
                    'name'=>'admin.clients.show',
                    'slug'=>'admin.clients.show',
                    'description'=>'show des clients',
                ],
                [
                    'name'=>'admin.clients.activate',
                    'slug'=>'admin.clients.activate',
                    'description'=>'activate des clients',
                ],
                [
                    'name'=>'admin.clients.agences',
                    'slug'=>'admin.clients.agences',
                    'description'=>'agences des clients',
                ],
                [
                    'name'=>'admin.clients.attachRole',
                    'slug'=>'admin.clients.attachRole',
                    'description'=>'attachRole des clients',
                ],
                [
                    'name'=>'admin.clients.desactivate',
                    'slug'=>'admin.clients.desactivate',
                    'description'=>'desactivate des clients',
                ],
                [
                    'name'=>'admin.clients.dettachRole',
                    'slug'=>'admin.clients.dettachRole',
                    'description'=>'dettachRole des clients',
                ],
                [
                    'name'=>'admin.clients.employees',
                    'slug'=>'admin.clients.employees',
                    'description'=>'employees des clients',
                ],
                [
                    'name'=>'admin.clients.garages',
                    'slug'=>'admin.clients.garages',
                    'description'=>'garages des clients',
                ],
                [
                    'name'=>'admin.clients.locations',
                    'slug'=>'admin.clients.locations',
                    'description'=>'locations des clients',
                ],
                [
                    'name'=>'admin.clients.reservations',
                    'slug'=>'admin.clients.reservations',
                    'description'=>'reservations des clients',
                ],
                [
                    'name'=>'admin.clients.roles',
                    'slug'=>'admin.clients.roles',
                    'description'=>'roles des clients',
                ],
                [
                    'name'=>'admin.clients.vehicules',
                    'slug'=>'admin.clients.vehicules',
                    'description'=>'vehicules des clients',
                ],
                [
                    'name'=>'admin.employees.store',
                    'slug'=>'admin.employees.store',
                    'description'=>'store des employee',
                ],
                [
                    'name'=>'admin.employees.index',
                    'slug'=>'admin.employees.index',
                    'description'=>'index des employee',
                ],
                [
                    'name'=>'admin.employees.destroy',
                    'slug'=>'admin.employees.destroy',
                    'description'=>'destroy des employee',
                ],
                [
                    'name'=>'admin.employees.update',
                    'slug'=>'admin.employees.update',
                    'description'=>'update des employee',
                ],
                [
                    'name'=>'admin.employees.show',
                    'slug'=>'admin.employees.show',
                    'description'=>'show des employee',
                ],
                [
                    'name'=>'admin.employees.activate',
                    'slug'=>'admin.employees.activate',
                    'description'=>'activate des employee',
                ],
                [
                    'name'=>'admin.employees.agences',
                    'slug'=>'admin.employees.agences',
                    'description'=>'agences des employee',
                ],
                [
                    'name'=>'admin.employees.attachRole',
                    'slug'=>'admin.employees.attachRole',
                    'description'=>'attachRole des employee',
                ],
                [
                    'name'=>'admin.employees.clients',
                    'slug'=>'admin.employees.clients',
                    'description'=>'clients des employee',
                ],
                [
                    'name'=>'admin.employees.desactivate',
                    'slug'=>'admin.employees.desactivate',
                    'description'=>'desactivate des employee',
                ],
                [
                    'name'=>'admin.employees.dettachRole',
                    'slug'=>'admin.employees.dettachRole',
                    'description'=>'dettachRole des employee',
                ],
                [
                    'name'=>'admin.employees.garages',
                    'slug'=>'admin.employees.garages',
                    'description'=>'garages des employee',
                ],
                [
                    'name'=>'admin.employees.locations',
                    'slug'=>'admin.employees.locations',
                    'description'=>'locations des employee',
                ],
                [
                    'name'=>'admin.employees.reservations',
                    'slug'=>'admin.employees.reservations',
                    'description'=>'reservations des employee',
                ],
                [
                    'name'=>'admin.employees.roles',
                    'slug'=>'admin.employees.roles',
                    'description'=>'roles des employee',
                ],
                [
                    'name'=>'admin.employees.vehicules',
                    'slug'=>'admin.employees.vehicules',
                    'description'=>'vehicules des employee',
                ],
                [
                    'name'=>'admin.garages.store',
                    'slug'=>'admin.garages.store',
                    'description'=>'stockage des garages',
                ],
                [
                    'name'=>'admin.garages.index',
                    'slug'=>'admin.garages.index',
                    'description'=>'index des garages',
                ],
                [
                    'name'=>'admin.garages.destroy',
                    'slug'=>'admin.garages.destroy',
                    'description'=>'destroy des garages',
                ],
                [
                    'name'=>'admin.garages.update',
                    'slug'=>'admin.garages.update',
                    'description'=>'update des garages',
                ],
                [
                    'name'=>'admin.garages.show',
                    'slug'=>'admin.garages.show',
                    'description'=>'show des garages',
                ],
                [
                    'name'=>'admin.garages.activate',
                    'slug'=>'admin.garages.activate',
                    'description'=>'acticate des garages',
                ],
                [
                    'name'=>'admin.garages.attachEmployee',
                    'slug'=>'admin.garages.attachEmployee',
                    'description'=>'attachEmployee des garages',
                ],
                [
                    'name'=>'admin.garages.clients',
                    'slug'=>'admin.garages.clients',
                    'description'=>'clients des garages',
                ],
                [
                    'name'=>'admin.garages.desactivate',
                    'slug'=>'admin.garages.desactivate',
                    'description'=>'desactivate des garages',
                ],
                [
                    'name'=>'admin.garages.dettachEmployee',
                    'slug'=>'admin.garages.dettachEmployee',
                    'description'=>'dettachEmployee des garages',
                ],
                [
                    'name'=>'admin.garages.employees',
                    'slug'=>'admin.garages.employees',
                    'description'=>'employees des garages',
                ],
                [
                    'name'=>'admin.garages.locations',
                    'slug'=>'admin.garages.locations',
                    'description'=>'locations des garages',
                ],
                [
                    'name'=>'admin.garages.marques',
                    'slug'=>'admin.garages.marques',
                    'description'=>'marques des garages',
                ],
                [
                    'name'=>'admin.garages.modeles',
                    'slug'=>'admin.garages.modeles',
                    'description'=>'modeles des garages',
                ],
                [
                    'name'=>'admin.garages.reservations',
                    'slug'=>'admin.garages.reservations',
                    'description'=>'reservations des garages',
                ],
                [
                    'name'=>'admin.garages.vehicules',
                    'slug'=>'admin.garages.vehicules',
                    'description'=>'vehicules des garages',
                ],
                [
                    'name'=>'admin.locations.index',
                    'slug'=>'admin.locations.index',
                    'description'=>'index des locations',
                ],
                [
                    'name'=>'admin.locations.store',
                    'slug'=>'admin.locations.store',
                    'description'=>'store des locations',
                ],
                [
                    'name'=>'admin.locations.destroy',
                    'slug'=>'admin.locations.destroy',
                    'description'=>'destroy des locations',
                ],
                [
                    'name'=>'admin.locations.show',
                    'slug'=>'admin.locations.show',
                    'description'=>'show des locations',
                ],
                [
                    'name'=>'admin.locations.update',
                    'slug'=>'admin.locations.update',
                    'description'=>'update des locations',
                ],
                [
                    'name'=>'admin.locations.clients',
                    'slug'=>'admin.locations.clients',
                    'description'=>'clients des locations',
                ],
                [
                    'name'=>'admin.locations.employees',
                    'slug'=>'admin.locations.employees',
                    'description'=>'employees des locations',
                ],
                [
                    'name'=>'admin.locations.ignore',
                    'slug'=>'admin.locations.ignore',
                    'description'=>'ignore des locations',
                ],
                [
                    'name'=>'admin.locations.toLocation',
                    'slug'=>'admin.locations.toLocation',
                    'description'=>'toLocation des locations',
                ],
                [
                    'name'=>'admin.locations.validate',
                    'slug'=>'admin.locations.validate',
                    'description'=>'validate des locations',
                ],
                [
                    'name'=>'admin.marques.index',
                    'slug'=>'admin.marques.index',
                    'description'=>'index des marques',
                ],
                [
                    'name'=>'admin.marques.store',
                    'slug'=>'admin.marques.store',
                    'description'=>'store des marques',
                ],
                [
                    'name'=>'admin.marques.destroy',
                    'slug'=>'admin.marques.destroy',
                    'description'=>'destroy des marques',
                ],
                [
                    'name'=>'admin.marques.show',
                    'slug'=>'admin.marques.show',
                    'description'=>'show des marques',
                ],
                [
                    'name'=>'admin.marques.update',
                    'slug'=>'admin.marques.update',
                    'description'=>'update des marques',
                ],
                [
                    'name'=>'admin.marques.agences',
                    'slug'=>'admin.marques.agences',
                    'description'=>'agences des marques',
                ],
                [
                    'name'=>'admin.marques.garages',
                    'slug'=>'admin.marques.garages',
                    'description'=>'garages des marques',
                ],
                [
                    'name'=>'admin.marques.locations',
                    'slug'=>'admin.marques.locations',
                    'description'=>'locations des marques',
                ],
                [
                    'name'=>'admin.marques.modeles',
                    'slug'=>'admin.marques.modeles',
                    'description'=>'modeles des marques',
                ],
                [
                    'name'=>'admin.marques.reservations',
                    'slug'=>'admin.marques.reservations',
                    'description'=>'reservations des marques',
                ],
                [
                    'name'=>'admin.marques.vehicules',
                    'slug'=>'admin.marques.vehicules',
                    'description'=>'vehicules des marques',
                ],
                [
                    'name'=>'admin.modeles.index',
                    'slug'=>'admin.modeles.index',
                    'description'=>'index des modeles',
                ],
                [
                    'name'=>'admin.modeles.store',
                    'slug'=>'admin.modeles.store',
                    'description'=>'store des modeles',
                ],
                [
                    'name'=>'admin.modeles.update',
                    'slug'=>'admin.modeles.update',
                    'description'=>'update des modeles',
                ],
                [
                    'name'=>'admin.modeles.destroy',
                    'slug'=>'admin.modeles.destroy',
                    'description'=>'destroy des modeles',
                ],
                [
                    'name'=>'admin.modeles.show',
                    'slug'=>'admin.modeles.show',
                    'description'=>'show des modeles',
                ],
                [
                    'name'=>'admin.modeles.agences',
                    'slug'=>'admin.modeles.agences',
                    'description'=>'agences des modeles',
                ],
                [
                    'name'=>'admin.modeles.garages',
                    'slug'=>'admin.modeles.garages',
                    'description'=>'garages des modeles',
                ],
                [
                    'name'=>'admin.modeles.locations',
                    'slug'=>'admin.modeles.locations',
                    'description'=>'locations des modeles',
                ],
                [
                    'name'=>'admin.modeles.reservations',
                    'slug'=>'admin.modeles.reservations',
                    'description'=>'reservations des modeles',
                ],
                [
                    'name'=>'admin.modeles.vehicules',
                    'slug'=>'admin.modeles.vehicules',
                    'description'=>'vehicules des modeles',
                ],
                [
                    'name'=>'admin.permissions.index',
                    'slug'=>'admin.permissions.index',
                    'description'=>'index des permissions',
                ],
                [
                    'name'=>'admin.permissions.store',
                    'slug'=>'admin.permissions.store',
                    'description'=>'store des permissions',
                ],
                [
                    'name'=>'admin.permissions.show',
                    'slug'=>'admin.permissions.show',
                    'description'=>'show des permissions',
                ],
                [
                    'name'=>'admin.permissions.destroy',
                    'slug'=>'admin.permissions.destroy',
                    'description'=>'destroy des permissions',
                ],
                [
                    'name'=>'admin.permissions.update',
                    'slug'=>'admin.permissions.update',
                    'description'=>'update des permissions',
                ],
                [
                    'name'=>'admin.permissions.attachPermission',
                    'slug'=>'admin.permissions.attachPermission',
                    'description'=>'attachPermission des permissions',
                ],
                [
                    'name'=>'admin.permissions.dettachPermission',
                    'slug'=>'admin.permissions.dettachPermission',
                    'description'=>'dettachPermission des permissions',
                ],
                [
                    'name'=>'admin.permissions.roles',
                    'slug'=>'admin.permissions.roles',
                    'description'=>'roles des permissions',
                ],
                [
                    'name'=>'admin.reservations.store',
                    'slug'=>'admin.reservations.store',
                    'description'=>'store des reservations',
                ],
                [
                    'name'=>'admin.reservations.index',
                    'slug'=>'admin.reservations.index',
                    'description'=>'index des reservations',
                ],
                [
                    'name'=>'admin.reservations.show',
                    'slug'=>'admin.reservations.show',
                    'description'=>'show des reservations',
                ],
                [
                    'name'=>'admin.reservations.update',
                    'slug'=>'admin.reservations.update',
                    'description'=>'update des reservations',
                ],
                [
                    'name'=>'admin.reservations.destroy',
                    'slug'=>'admin.reservations.destroy',
                    'description'=>'destroy des reservations',
                ],
                [
                    'name'=>'admin.reservations.clients',
                    'slug'=>'admin.reservations.clients',
                    'description'=>'clients des reservations',
                ],
                [
                    'name'=>'admin.reservations.clients',
                    'slug'=>'admin.reservations.clients',
                    'description'=>'clients des reservations',
                ],
                [
                    'name'=>'admin.reservations.employees',
                    'slug'=>'admin.reservations.employees',
                    'description'=>'employees des reservations',
                ],
                [
                    'name'=>'admin.reservations.ignore',
                    'slug'=>'admin.reservations.ignore',
                    'description'=>'ignore des reservations',
                ],
                [
                    'name'=>'admin.reservations.toLocation',
                    'slug'=>'admin.reservations.toLocation',
                    'description'=>'toLocation des reservations',
                ],
                [
                    'name'=>'admin.reservations.validate',
                    'slug'=>'admin.reservations.validate',
                    'description'=>'validate des reservations',
                ],
                [
                    'name'=>'admin.roles.index',
                    'slug'=>'admin.roles.index',
                    'description'=>'index des roles',
                ],
                [
                    'name'=>'admin.roles.store',
                    'slug'=>'admin.roles.store',
                    'description'=>'store des roles',
                ],
                [
                    'name'=>'admin.roles.update',
                    'slug'=>'admin.roles.update',
                    'description'=>'update des roles',
                ],
                [
                    'name'=>'admin.roles.destroy',
                    'slug'=>'admin.roles.destroy',
                    'description'=>'destroy des roles',
                ],
                [
                    'name'=>'admin.roles.show',
                    'slug'=>'admin.roles.show',
                    'description'=>'show des roles',
                ],
                [
                    'name'=>'admin.roles.attachRole',
                    'slug'=>'admin.roles.attachRole',
                    'description'=>'attachRole des roles',
                ],
                [
                    'name'=>'admin.roles.dettachRole',
                    'slug'=>'admin.roles.dettachRole',
                    'description'=>'dettachRole des roles',
                ],
                [
                    'name'=>'admin.roles.permissions',
                    'slug'=>'admin.roles.permissions',
                    'description'=>'permissions des roles',
                ],
                [
                    'name'=>'admin.tariffs.index',
                    'slug'=>'admin.tariffs.index',
                    'description'=>'index des tariffs',
                ],
                [
                    'name'=>'admin.tariffs.store',
                    'slug'=>'admin.tariffs.store',
                    'description'=>'store des tariffs',
                ],
                [
                    'name'=>'admin.tariffs.destroy',
                    'slug'=>'admin.tariffs.destroy',
                    'description'=>'destroy des tariffs',
                ],
                [
                    'name'=>'admin.tariffs.update',
                    'slug'=>'admin.tariffs.update',
                    'description'=>'update des tariffs',
                ],
                [
                    'name'=>'admin.tariffs.show',
                    'slug'=>'admin.tariffs.show',
                    'description'=>'show des tariffs',
                ],
                [
                    'name'=>'admin.vehicules.store',
                    'slug'=>'admin.vehicules.store',
                    'description'=>'store des vehicules',
                ],
                [
                    'name'=>'admin.vehicules.index',
                    'slug'=>'admin.vehicules.index',
                    'description'=>'index des vehicules',
                ],
                [
                    'name'=>'admin.vehicules.update',
                    'slug'=>'admin.vehicules.update',
                    'description'=>'update des vehicules',
                ],
                [
                    'name'=>'admin.vehicules.destroy',
                    'slug'=>'admin.vehicules.destroy',
                    'description'=>'destroy des vehicules',
                ],
                [
                    'name'=>'admin.vehicules.show',
                    'slug'=>'admin.vehicules.show',
                    'description'=>'show des vehicules',
                ],
                [
                    'name'=>'admin.vehicules.clients',
                    'slug'=>'admin.vehicules.clients',
                    'description'=>'clients des vehicules',
                ],
                [
                    'name'=>'admin.vehicules.employees',
                    'slug'=>'admin.vehicules.employees',
                    'description'=>'employees des vehicules',
                ],
                [
                    'name'=>'admin.vehicules.locations',
                    'slug'=>'admin.vehicules.locations',
                    'description'=>'locations des vehicules',
                ],
                [
                    'name'=>'admin.vehicules.reservations',
                    'slug'=>'admin.vehicules.reservations',
                    'description'=>'reservations des vehicules',
                ],
                [
                    'name'=>'client.agences.index',
                    'slug'=>'client.agences.index',
                    'description'=>'index des agences',
                ],
                [
                    'name'=>'client.agences.show',
                    'slug'=>'client.agences.show',
                    'description'=>'show des agences',
                ],
                [
                    'name'=>'client.agences.garages',
                    'slug'=>'client.agences.garages',
                    'description'=>'garages des agences',
                ],
                [
                    'name'=>'client.agences.locations',
                    'slug'=>'client.agences.locations',
                    'description'=>'locations des agences',
                ],
                [
                    'name'=>'client.agences.marques',
                    'slug'=>'client.agences.marques',
                    'description'=>'marques des agences',
                ],
                [
                    'name'=>'client.agences.modeles',
                    'slug'=>'client.agences.modeles',
                    'description'=>'modeles des agences',
                ],
                [
                    'name'=>'client.agences.reservations',
                    'slug'=>'client.agences.reservations',
                    'description'=>'reservations des agences',
                ],
                [
                    'name'=>'client.agences.vehicules',
                    'slug'=>'client.agences.vehicules',
                    'description'=>'vehicules des agences',
                ],
                [
                    'name'=>'client.garages.index',
                    'slug'=>'client.garages.index',
                    'description'=>'index des garages',
                ],
                [
                    'name'=>'client.garages.show',
                    'slug'=>'client.garages.show',
                    'description'=>'show des garages',
                ],
                [
                    'name'=>'client.garages.locations',
                    'slug'=>'client.garages.locations',
                    'description'=>'locations des garages',
                ],
                [
                    'name'=>'client.garages.marques',
                    'slug'=>'client.garages.marques',
                    'description'=>'marques des garages',
                ],
                [
                    'name'=>'client.garages.modeles',
                    'slug'=>'client.garages.modeles',
                    'description'=>'modeles des garages',
                ],
                [
                    'name'=>'client.garages.reservations',
                    'slug'=>'client.garages.reservations',
                    'description'=>'reservations des garages',
                ],
                [
                    'name'=>'client.garages.vehicules',
                    'slug'=>'client.garages.vehicules',
                    'description'=>'vehicules des garages',
                ],
                [
                    'name'=>'client.locations.index',
                    'slug'=>'client.locations.index',
                    'description'=>'index des locations',
                ],
                [
                    'name'=>'client.locations.show',
                    'slug'=>'client.locations.show',
                    'description'=>'show des locations',
                ],
                [
                    'name'=>'client.marques.agences',
                    'slug'=>'client.marques.agences',
                    'description'=>'agences des marques',
                ],
                [
                    'name'=>'client.marques.garages',
                    'slug'=>'client.marques.garages',
                    'description'=>'garages des marques',
                ],
                [
                    'name'=>'client.marques.locations',
                    'slug'=>'client.marques.locations',
                    'description'=>'locations des marques',
                ],
                [
                    'name'=>'client.marques.modeles',
                    'slug'=>'client.marques.modeles',
                    'description'=>'modeles des marques',
                ],
                [
                    'name'=>'client.marques.reservations',
                    'slug'=>'client.marques.reservations',
                    'description'=>'reservations des marques',
                ],
                [
                    'name'=>'client.modeles.index',
                    'slug'=>'client.modeles.index',
                    'description'=>'index des modeles',
                ],
                [
                    'name'=>'client.modeles.show',
                    'slug'=>'client.modeles.show',
                    'description'=>'show des modeles',
                ],
                [
                    'name'=>'client.modeles.agences',
                    'slug'=>'client.modeles.agences',
                    'description'=>'agences des modeles',
                ],
                [
                    'name'=>'client.modeles.garages',
                    'slug'=>'client.modeles.garages',
                    'description'=>'garages des modeles',
                ],
                [
                    'name'=>'client.modeles.locations',
                    'slug'=>'client.modeles.locations',
                    'description'=>'locations des modeles',
                ],
                [
                    'name'=>'client.modeles.reservations',
                    'slug'=>'client.modeles.reservations',
                    'description'=>'reservations des modeles',
                ],
                [
                    'name'=>'client.modeles.vehicules',
                    'slug'=>'client.modeles.vehicules',
                    'description'=>'vehicules des modeles',
                ],
                [
                    'name'=>'client.reservations.store',
                    'slug'=>'client.reservations.store',
                    'description'=>'store des reservations',
                ],
                [
                    'name'=>'client.reservations.index',
                    'slug'=>'client.reservations.index',
                    'description'=>'index des reservations',
                ],
                [
                    'name'=>'client.reservations.destroy',
                    'slug'=>'client.reservations.destroy',
                    'description'=>'destroy des reservations',
                ],
                [
                    'name'=>'client.reservations.update',
                    'slug'=>'client.reservations.update',
                    'description'=>'update des reservations',
                ],
                [
                    'name'=>'client.reservations.show',
                    'slug'=>'client.reservations.show',
                    'description'=>'show des reservations',
                ],
                [
                    'name'=>'client.reservations.ignore',
                    'slug'=>'client.reservations.ignore',
                    'description'=>'ignore des reservations',
                ],
                [
                    'name'=>'client.reservations.toLocation',
                    'slug'=>'client.reservations.toLocation',
                    'description'=>'toLocation des reservations',
                ],
                [
                    'name'=>'client.tariffs.index',
                    'slug'=>'client.tariffs.index',
                    'description'=>'index des tariffs',
                ],
                [
                    'name'=>'client.tariffs.show',
                    'slug'=>'client.tariffs.show',
                    'description'=>'show des tariffs',
                ],
                [
                    'name'=>'client.vehicules.index',
                    'slug'=>'client.vehicules.index',
                    'description'=>'index des vehicules',
                ],
                [
                    'name'=>'client.vehicules.show',
                    'slug'=>'client.vehicules.show',
                    'description'=>'show des vehicules',
                ],
                [
                    'name'=>'client.vehicules.locations',
                    'slug'=>'client.vehicules.locations',
                    'description'=>'locations des vehicules',
                ],
                [
                    'name'=>'client.vehicules.reservations',
                    'slug'=>'client.vehicules.reservations',
                    'description'=>'reservations des vehicules',
                ],
                [
                    'name'=>'guest.agences.index',
                    'slug'=>'guest.agences.index',
                    'description'=>'index des agences',
                ],
                [
                    'name'=>'guest.agences.show',
                    'slug'=>'guest.agences.show',
                    'description'=>'show des agences',
                ],
                [
                    'name'=>'guest.agences.garages',
                    'slug'=>'guest.agences.garages',
                    'description'=>'garages des agences',
                ],
                [
                    'name'=>'guest.agences.marques',
                    'slug'=>'guest.agences.marques',
                    'description'=>'marques des agences',
                ],
                [
                    'name'=>'guest.agences.modeles',
                    'slug'=>'guest.agences.modeles',
                    'description'=>'modeles des agences',
                ],
                [
                    'name'=>'guest.agences.vehicules',
                    'slug'=>'guest.agences.vehicules',
                    'description'=>'vehicules des agences',
                ],
                [
                    'name'=>'guest.garages.index',
                    'slug'=>'guest.garages.index',
                    'description'=>'index des garages',
                ],
                [
                    'name'=>'guest.garages.show',
                    'slug'=>'guest.garages.show',
                    'description'=>'show des garages',
                ],
                [
                    'name'=>'guest.garages.marques',
                    'slug'=>'guest.garages.marques',
                    'description'=>'marques des garages',
                ],
                [
                    'name'=>'guest.garages.modeles',
                    'slug'=>'guest.garages.modeles',
                    'description'=>'modeles des garages',
                ],
                [
                    'name'=>'guest.garages.vehicules',
                    'slug'=>'guest.garages.vehicules',
                    'description'=>'vehicules des garages',
                ],
                [
                    'name'=>'guest.marques.index',
                    'slug'=>'guest.marques.index',
                    'description'=>'index des marques',
                ],
                [
                    'name'=>'guest.marques.show',
                    'slug'=>'guest.marques.show',
                    'description'=>'show des marques',
                ],
                [
                    'name'=>'guest.marques.agences',
                    'slug'=>'guest.marques.agences',
                    'description'=>'agences des marques',
                ],
                [
                    'name'=>'guest.marques.garages',
                    'slug'=>'guest.marques.garages',
                    'description'=>'garages des marques',
                ],
                [
                    'name'=>'guest.marques.modeles',
                    'slug'=>'guest.marques.modeles',
                    'description'=>'modeles des marques',
                ],
                [
                    'name'=>'guest.modeles.index',
                    'slug'=>'guest.modeles.index',
                    'description'=>'index des modeles',
                ],
                [
                    'name'=>'guest.modeles.show',
                    'slug'=>'guest.modeles.show',
                    'description'=>'show des modeles',
                ],
                [
                    'name'=>'guest.modeles.agences',
                    'slug'=>'guest.modeles.agences',
                    'description'=>'agences des modeles',
                ],
                [
                    'name'=>'guest.modeles.garages',
                    'slug'=>'guest.modeles.garages',
                    'description'=>'garages des modeles',
                ],
                [
                    'name'=>'guest.modeles.vehicules',
                    'slug'=>'guest.modeles.vehicules',
                    'description'=>'vehicules des modeles',
                ],
                [
                    'name'=>'guest.tariffs.index',
                    'slug'=>'guest.tariffs.index',
                    'description'=>'index des tariffs',
                ],
                [
                    'name'=>'guest.tariffs.show',
                    'slug'=>'guest.tariffs.show',
                    'description'=>'show des tariffs',
                ],
                [
                    'name'=>'guest.vehicules.index',
                    'slug'=>'guest.vehicules.index',
                    'description'=>'index des vehicules',
                ],
                [
                    'name'=>'guest.vehicules.show',
                    'slug'=>'guest.vehicules.show',
                    'description'=>'show des vehicules',
                ]
            ]
        );
    }
}
