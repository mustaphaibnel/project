<?php

namespace App\Policies;

use App\User;
use App\Models\Garage;
use Illuminate\Auth\Access\HandlesAuthorization;

class GaragePolicy
{
    use HandlesAuthorization;

    /**
     * Determine whether the user can view the garage.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Garage  $garage
     * @return mixed
     */
    public function view(User $user, Garage $garage)
    {

    }
    public function show(User $user, Garage $garage)
    {
        return $user->ownedGarage($garage->id);
    }
    public function users(User $user, Garage $garage)
    {
        return $user->ownedGarage($garage->id);
    }
    public function vehicules(User $user, Garage $garage)
    {
        return $user->ownedGarage($garage->id);
    }
    public function clients(User $user, Garage $garage)
    {
        return $user->ownedGarage($garage->id);
    }
    public function reservations(User $user, Garage $garage)
    {
        return $user->ownedGarage($garage->id);
    }
    public function locations(User $user, Garage $garage)
    {
        return $user->ownedGarage($garage->id);
    }
    public function garages(User $user, Garage $garage)
    {
        return $user->ownedGarage($garage->id);
    }
    public function modeles(User $user, Garage $garage)
    {
        return $user->ownedGarage($garage->id);
    }
    public function marques(User $user, Garage $garage)
    {
        return $user->ownedGarage($garage->id);
    }

    /**
     * Determine whether the user can create garages.
     *
     * @param  \App\User  $user
     * @return mixed
     */

    public function create(User $user)
    {
        //
    }

    /**
     * Determine whether the user can update the garage.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Garage  $garage
     * @return mixed
     */
    public function update(User $user, Garage $garage)
    {
        //
    }

    /**
     * Determine whether the user can delete the garage.
     *
     * @param  \App\User  $user
     * @param  \App\Models\Garage  $garage
     * @return mixed
     */
    public function delete(User $user, Garage $garage)
    {
        //
    }
}
