<?php

use Illuminate\Database\Seeder;

class PermissionRoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 0; $i < 200; $i++) {
            DB::table('permission_role')->insert(
                [
                    [
                        'role_id' => App\Models\Role::all()->random()->id,
                        'permission_id' =>  App\Models\Permission::all()->random()->id,
                    ],

                ]);
        }

    }
}
