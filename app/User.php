<?php

namespace App;

use App\Models\Garage;
use App\Models\Role;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use  Notifiable;
    /**
     * The attributes that are mass assignable.
     *
     * @var array



     */

    protected $table='users';
    protected $fillable = [
        'name', 'email', 'password','verified','activated','type'
    ];



    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];



    public function OauthAcessToken(){
        return $this->hasMany();
    }
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }
    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function assignRole(Role $role)
    {
        return $this->roles()->save($role);
    }

    public function hasRole($role)
    {
        if (is_string($role)) {
            return $this->roles->contains('name', $role);
        }
        return !! $role->intersect($this->roles)->count();
    }

    /**
     * Determine if the user may perform the given permission.
     *
     * @param  Permission $permission
     * @return boolean
     */
    public function hasPermission($permission)
    {
        $roles=$this->roles;
        foreach ($roles as $role){
            if ($role->inRole($permission))
            {
                return true;
            }
            return false;
        }
    }

    public function garages()
    {
        return $this->morphToMany(Garage::class, 'garageable');
    }

    public function vehicules()
    {
        return $vehicules =User::find($this->id)->garages()->with('vehicules')->get()->pluck('vehicules')->collapse();
    }
    public function locations()
    {
        return $locations =User::find($this->id)->garages()->with('vehicules.locations')->get()->pluck('locations')->collapse();
    }
    public function reservations()
    {
        return $reservations =User::find($this->id)->garages()->with('vehicules.reservations')->get()->pluck('reservations')->collapse();
    }
    public function agences()
    {
        return $agences =User::find($this->id)->garages()->with('agence')->get()->pluck('agence')->unique('id');


    }

    public function ownerGarage($garageId){

        dd($garageId);
        if ($garageId) {
            return $this->garages->contains('id', $garageId);
        }
        return !! $garageId->intersect($this->garages)->count();
    }
    public function permissions(){

        return $permissions =User::find($this->id)->roles()->with('permissions')->get()->pluck('permissions')->unique('id')->all();

    }

    public function isUser(){
       return  !! $user=User::whereHas('garages')->find($this->id);
    }

    public function ownedAgence($id){

        return $this->agences()->contains('id',$id);

    }
    public function ownedReservation($id){

        return $this->vehicules()->contains('id',$id);

    }
    public function ownedLocation($id){

        return $this->vehicules()->contains('id',$id);

    }
    public function ownedVehicule($id){

        return $this->vehicules()->contains('id',$id);

    }
    public function ownedGarage($id){
        return $this->garages->contains('id',$id);
    }
}
