<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Modele extends Model
{
    public function vehicules(){
        return $this->hasMany('App\Models\Vehicule');
    }
    public function marque(){
        return $this->belongsTo('App\Models\Marque');
    }
    public function agences()
    {
        return $this->vehicules()->get()->pluck('agence');
    }
    public function garages()
    {
        return $this->vehicules->pluck('garage');
    }
    public function reservations()
    {
        return $reservations =Modele::find($this->id)->vehicules()->with('reservations')->get()->pluck('reservations')->collapse()->unique('id')->all();
    }
    public function locations()
    {
        return $locations =Modele::find($this->id)->vehicules()->with('locations')->get()->pluck('locations')->collapse()->unique('id')->all();
    }
}
