<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reservations', function (Blueprint $table) {
            $table->increments('id');
            $table->boolean('gps');
            $table->boolean('conducteur');
            $table->boolean('siege_bebe');
            $table->dateTime('date_debut');
            $table->dateTime('date_fin');
            $table->dateTime('validate_at')->nullable();
            $table->integer('user_id')->nullable();
            $table->unsignedInteger('vehicule_id');
            $table->unsignedInteger('client_id');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reservations');
    }
}
