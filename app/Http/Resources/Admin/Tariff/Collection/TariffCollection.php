<?php

namespace App\Http\Resources\Admin\Tariff\Collection;

use Illuminate\Http\Resources\Json\JsonResource;

class TariffCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return parent::toArray($request);
    }
}
