<?php

namespace App\Http\Resources\Admin\Modele\Collection;

use Illuminate\Http\Resources\Json\JsonResource;

class ModeleCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'name'=> (string) $this->name,
            'slug'=> (string) $this->slug,
            'marque'=> (string) $this->marque->name,
            'created_at'=>(string) $this->created_at->diffForHumans(),
            'updated_at'=>(string) $this->updated_at->diffForHumans(),
            'link'=>[
                'self'=>Route('admin.modeles.show',$this->id),

            ],
        ];
    }
}
