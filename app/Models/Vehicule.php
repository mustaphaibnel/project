<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Traits\Publishable;

class Vehicule extends Model
{
    use Publishable;
    protected $guarded = [];
    public function tariffs(){
        return $this->hasMany(Tariff::class);
    }
    public function modele(){
        return $this->belongsTo(Modele::class);
    }
    public function marque(){
        return $this->modele->marque;
    }
    public function agence(){
        return $this->garage->agence;
    }
    public function garage(){
        return $this->belongsTo(Garage::class);
    }
    public function reservations(){
        return $this->hasMany(Reservation::class);
    }
    public function locations(){
        return $this->hasMany(Location::class);
    }
}
