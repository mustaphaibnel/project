<?php

namespace App\Http\Resources\Admin\Vehicule\Collection;

use Illuminate\Http\Resources\Json\JsonResource;

class VehiculeCollection extends JsonResource
{
    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id'=>$this->id,
            'marque'=>$this->marque()->name,
            'modele'=>$this->modele->name,
            'garage'=>$this->garage->name,
            'agence'=>$this->garage->agence->name,
            'vitesse_automatique'=>$this->vitesse_automatique,
            'promotion'=>$this->promotion,
            'links'=>[
                'self'=>Route('admin.locations.show',$this->id),
            ],
        ];
    }
}
