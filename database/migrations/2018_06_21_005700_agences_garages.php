<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AgencesGarages extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('agences_garages', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('slug');
            $table->string('addresse');
            $table->string('code_postale');
            $table->string('telephone_mobile');
            $table->string('telephone_fix');
            $table->string('ville');
            $table->string('site_web');
            $table->string('facebook_page');
            $table->text('geolocalisation');
            $table->string('image');
            $table->integer('agence_id')->nullable();
            $table->dateTime('activated')->nullable();
            $table->dateTime('verified')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('agences_garages');
    }
}
