<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;

trait Veriviable
{
    /**
     * Scope a query to only include verified models.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeVerivied(Builder $query)
    {
        return $query->where('verified_at', '<=', Carbon::now())->whereNotNull('verified_at');
    }

    /**
     * Scope a query to only include unverified models.
     *
     * @param  \Illuminate\Database\Eloquent\Builder $query
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function scopeUnverified(Builder $query)
    {
        return $query->where('verified_at', '>', Carbon::now())->orWhereNull('verified_at');
    }

    /**
     * @return bool
     */
    public function isVerivied()
    {
        if (is_null($this->verified_at)) {
            return false;
        }

        return $this->verified_at->lte(Carbon::now());
    }

    /**
     * @return bool
     */
    public function isUnverified()
    {
        return !$this->isVerivied();
    }

    /**
     * @return bool
     */
    public function verifie()
    {
        return $this->update([
            'verified_at' => Carbon::now()->toDateTimeString(),
        ]);
    }

    /**
     * @return bool
     */
    public function unverifie()
    {
        return $this->update([
            'verified_at' => null,
        ]);
    }

    /**
     * @param  mixed $value
     * @return \Carbon\Carbon
     */
    public function getVeriviedAtAttribute($value)
    {
        if (is_null($value)) {
            return $value;
        }

        return $this->asDateTime($value);
    }
}