<?php

namespace App\Http\Middleware\Custom;

use Closure;

class Activated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->user()->activated==Null){
            return response([['errors'=>"Unauthorized action.",'details'=>["type"=> 'unactivated',"message"=> 'utilisateur non activer']]], 403);
        }
        else{
            return $next($request);
        }
    }
}
